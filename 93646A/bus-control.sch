EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 17 22
Title ""
Date "2019-11-23"
Rev ""
Comp ""
Comment1 "Licence: CC-BY"
Comment2 "Author: Loïc *WydD* Petit"
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5050 2750
Connection ~ 3350 3850
Entry Wire Line
	3350 2550 3450 2450
Entry Wire Line
	3350 2650 3450 2550
Entry Wire Line
	3350 2750 3450 2650
Entry Wire Line
	3350 2850 3450 2750
Entry Wire Line
	3350 2950 3450 2850
Entry Wire Line
	3350 3050 3450 2950
Entry Wire Line
	3350 3150 3450 3050
Entry Wire Line
	3350 3250 3450 3150
Entry Wire Line
	3350 3350 3450 3250
Entry Wire Line
	6250 3050 6350 2950
Entry Wire Line
	6250 3150 6350 3050
Entry Wire Line
	6250 3250 6350 3150
Entry Wire Line
	6250 3350 6350 3250
Entry Wire Line
	6250 3450 6350 3350
Entry Wire Line
	6250 3550 6350 3450
Wire Wire Line
	3450 2350 4050 2350
Wire Wire Line
	3450 2450 4050 2450
Wire Wire Line
	3450 2550 4050 2550
Wire Wire Line
	3450 2650 4050 2650
Wire Wire Line
	3450 2750 4050 2750
Wire Wire Line
	3450 2850 4050 2850
Wire Wire Line
	3450 2950 4050 2950
Wire Wire Line
	3450 3050 4050 3050
Wire Wire Line
	3450 3150 4050 3150
Wire Wire Line
	3450 3250 4050 3250
Wire Wire Line
	5050 2550 6950 2550
Wire Wire Line
	5050 2750 5050 2850
Wire Wire Line
	5050 2750 5500 2750
Wire Wire Line
	5100 2950 5050 2950
Wire Wire Line
	5300 3050 5050 3050
Wire Wire Line
	6350 2650 6950 2650
Wire Wire Line
	6350 2750 6950 2750
Wire Wire Line
	6350 2850 6950 2850
Wire Wire Line
	6350 2950 6950 2950
Wire Wire Line
	6350 3050 6950 3050
Wire Wire Line
	6350 3150 6950 3150
Wire Wire Line
	6350 3250 6950 3250
Wire Wire Line
	6350 3350 6950 3350
Wire Wire Line
	6350 3450 6950 3450
Wire Bus Line
	3350 2550 3350 2750
Wire Bus Line
	3350 2750 3350 2950
Wire Bus Line
	3350 2950 3350 3050
Wire Bus Line
	3350 3050 3350 3250
Wire Bus Line
	3350 3250 3350 3850
Wire Bus Line
	3350 3850 3000 3850
Wire Bus Line
	6250 3050 6250 3250
Wire Bus Line
	6250 3250 6250 3350
Wire Bus Line
	6250 3350 6250 3550
Wire Bus Line
	6250 3550 6250 3850
Wire Bus Line
	6250 3850 3350 3850
Text Notes 3050 5250 0    50   ~ 0
/o12 = /i1 & i2 & i3 & i4 & i5 & i6 & i7 & i8 & i9 & /i14 & /i15 + /i13\n/o16 = /i1 & /i2 & i3 & i4 & /i5 & /i6 & i7 & i8\n/o17 = /i1 & i2 & /i3 & /i4 & /i5 & /i6\n/o18 = /i1 & /i2 & i3 & i4 & /i5 & /i6 & /i7 & i8\n/o19 = /i1 & /i2 & i3 & i4 & /i5 & /i6 & /i7 & /i8
Text Notes 3050 6100 0    50   ~ 0
/o12 = /BUS-E & ADDR=FFxxxx + /i13 // Stack RAM\n/o16 = /BUS-E & ADDR=(660000 > 67ffff) // Peripherals\n/o17 = /BUS-E & ADDR=(800000 > 87ffff) // Registers\n/o18 = /BUS-E & ADDR=(620000 > 63ffff) // ?? unused\n/o19 = /BUS-E & ADDR=(600000 > 61ffff) // QSound RAM\nADDR is in 8 bits to compare to MAME source
Text Notes 6300 5500 0    50   ~ 0
/o12 = /i1 & /i2 & /i5 & /i6 & /i7 & i8 & i9 & /i11\n/o13 = /i1 & i5 & /i6 & i7\n/o14 = /i1 & i5 & /i6 & /i7\n/o15 = /i1 & /i4 & /i5 & /i6 & i7 & /i8 & /i9 & /i11\n/o16 = /i1 & /i3 & /i5 & /i6 & i7 & /i8 & /i9 & /i11\n/o17 = /i1 & /i2 & /i5 & /i6 & /i7 & i8 & /i9 & /i11\n/o18 = /i1 & /i2 & /i5 & /i6 & /i7 & /i8 & i9 & /i11\n/o19 = /i1 & /i2 & /i5 & /i6 & /i7 & /i8 & /i9 & /i11
Text Notes 6300 6250 0    50   ~ 0
/o12 = /OE & ADDR.endsWith(30 - 37) // qsound volume\n/o13 = ADDR.endsWith(140 - 17F) // cps_b_regs\n/o14 = ADDR.endsWith(100 - 13F) // cps_a_regs\n/o15 = /WE & ADDR.endsWith(40 - 47) // coin lock\n/o16 = CONTROL1 & ADDR.endsWith(40 - 47) // eeprom\n/o17 = /OE & ADDR.endsWith(20 - 27) // IN2\n/o18 = /OE & ADDR.endsWith(10 - 17) // IN1\n/o19 = /OE & ADDR.endsWith(00 - 07) // IN0
Text Notes 8500 3000 0    50   ~ 0
IO45\nIO46\nIO47\n\nIO48\nU7H11
Text Label 3500 2350 0    50   ~ 0
~BUS-E
Text Label 3500 2450 0    50   ~ 0
BUS-ADDR23
Text Label 3500 2550 0    50   ~ 0
BUS-ADDR22
Text Label 3500 2650 0    50   ~ 0
BUS-ADDR21
Text Label 3500 2750 0    50   ~ 0
BUS-ADDR20
Text Label 3500 2850 0    50   ~ 0
BUS-ADDR19
Text Label 3500 2950 0    50   ~ 0
BUS-ADDR18
Text Label 3500 3050 0    50   ~ 0
BUS-ADDR17
Text Label 3500 3150 0    50   ~ 0
BUS-ADDR16
Text Label 3500 3250 0    50   ~ 0
BUS-ADDR15
Text Label 5700 2550 0    50   ~ 0
~REGISTERS-E
Text Label 6400 2950 0    50   ~ 0
BUS-ADDR8
Text Label 6400 3050 0    50   ~ 0
BUS-ADDR7
Text Label 6400 3150 0    50   ~ 0
BUS-ADDR6
Text Label 6400 3250 0    50   ~ 0
BUS-ADDR5
Text Label 6400 3350 0    50   ~ 0
BUS-ADDR4
Text Label 6400 3450 0    50   ~ 0
BUS-ADDR3
Text HLabel 3000 3850 0    50   Input ~ 0
BUS-ADDR[3..23]
Text HLabel 3450 2350 0    50   Input ~ 0
~BUS-E
Text HLabel 5050 2350 2    50   Output ~ 0
~SOUND
Text HLabel 5050 2450 2    50   Output ~ 0
~PERIPHERALS
Text HLabel 5050 2650 2    50   Output ~ 0
~MISC
Text HLabel 5300 2950 2    50   Input ~ 0
IO101
Text HLabel 5300 3050 2    50   Output ~ 0
~STACK
Text HLabel 6350 2650 0    50   Input ~ 0
~BUS-OE
Text HLabel 6350 2750 0    50   Input ~ 0
~BUS-UDSW
Text HLabel 6350 2850 0    50   Input ~ 0
~BUS-WE
Text HLabel 7950 2550 2    50   Output ~ 0
~IN0
Text HLabel 7950 2650 2    50   Output ~ 0
~IN1
Text HLabel 7950 2750 2    50   Output ~ 0
~IN2
Text HLabel 7950 2850 2    50   Output ~ 0
~OUT-W
Text HLabel 7950 2950 2    50   Output ~ 0
~COIN-W
Text HLabel 7950 3050 2    50   Output ~ 0
~CPSA
Text HLabel 7950 3150 2    50   Output ~ 0
~CPSB
Text HLabel 7950 3250 2    50   Output ~ 0
~VOLUME
$Comp
L power:VCC #PWR0413
U 1 1 5E37E69B
P 4550 2150
F 0 "#PWR0413" H 4550 2000 50  0001 C CNN
F 1 "VCC" H 4567 2323 50  0000 C CNN
F 2 "" H 4550 2150 50  0001 C CNN
F 3 "" H 4550 2150 50  0001 C CNN
	1    4550 2150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0412
U 1 1 5E37E047
P 7450 2350
F 0 "#PWR0412" H 7450 2200 50  0001 C CNN
F 1 "VCC" H 7467 2523 50  0000 C CNN
F 2 "" H 7450 2350 50  0001 C CNN
F 3 "" H 7450 2350 50  0001 C CNN
	1    7450 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E0AF8BE
P 4550 3450
AR Path="/5E0AF8BE" Ref="#PWR?"  Part="1" 
AR Path="/5E0AD584/5E0AF8BE" Ref="#PWR0314"  Part="1" 
F 0 "#PWR0314" H 4550 3200 50  0001 C CNN
F 1 "GND" H 4555 3277 50  0000 C CNN
F 2 "" H 4550 3450 50  0001 C CNN
F 3 "" H 4550 3450 50  0001 C CNN
	1    4550 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E0AF8CE
P 5500 2750
AR Path="/5E0AF8CE" Ref="#PWR?"  Part="1" 
AR Path="/5E0AD584/5E0AF8CE" Ref="#PWR0316"  Part="1" 
F 0 "#PWR0316" H 5500 2500 50  0001 C CNN
F 1 "GND" H 5650 2700 50  0000 C CNN
F 2 "" H 5500 2750 50  0001 C CNN
F 3 "" H 5500 2750 50  0001 C CNN
	1    5500 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E0AF8C4
P 7450 3650
AR Path="/5E0AF8C4" Ref="#PWR?"  Part="1" 
AR Path="/5E0AD584/5E0AF8C4" Ref="#PWR0315"  Part="1" 
F 0 "#PWR0315" H 7450 3400 50  0001 C CNN
F 1 "GND" H 7455 3477 50  0000 C CNN
F 2 "" H 7450 3650 50  0001 C CNN
F 3 "" H 7450 3650 50  0001 C CNN
	1    7450 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB?
U 1 1 5E0AF8DF
P 5200 2950
AR Path="/5E0AF8DF" Ref="FB?"  Part="1" 
AR Path="/5E0AD584/5E0AF8DF" Ref="FB27"  Part="1" 
F 0 "FB27" V 5055 2950 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 5054 2950 50  0001 C CNN
F 2 "" V 5130 2950 50  0001 C CNN
F 3 "~" H 5200 2950 50  0001 C CNN
	1    5200 2950
	0    1    1    0   
$EndComp
$Comp
L Logic_Programmable:PAL16L8 UN?
U 1 1 5E0AF8B2
P 4550 2850
AR Path="/5E0AF8B2" Ref="UN?"  Part="1" 
AR Path="/5E0AD584/5E0AF8B2" Ref="UN13"  Part="1" 
F 0 "UN13" H 4300 3500 50  0000 C CNN
F 1 "PAL16L8 (BGSA1)" H 4950 2300 50  0000 C CNN
F 2 "" H 4550 2850 50  0001 C CNN
F 3 "" H 4550 2850 50  0001 C CNN
	1    4550 2850
	1    0    0    -1  
$EndComp
$Comp
L Logic_Programmable:PAL16L8 UN?
U 1 1 5E0AF8B8
P 7450 3050
AR Path="/5E0AF8B8" Ref="UN?"  Part="1" 
AR Path="/5E0AD584/5E0AF8B8" Ref="UN14"  Part="1" 
F 0 "UN14" H 7200 3700 50  0000 C CNN
F 1 "PAL16L8 (BGSA2)" H 7850 2500 50  0000 C CNN
F 2 "" H 7450 3050 50  0001 C CNN
F 3 "" H 7450 3050 50  0001 C CNN
	1    7450 3050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
