EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 17
Title ""
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 9800 3400
Connection ~ 9800 5000
Connection ~ 8800 4000
Connection ~ 9300 4300
Connection ~ 7650 3800
Connection ~ 6800 5800
Connection ~ 1750 7100
Connection ~ 1050 2950
Connection ~ 1050 3050
Connection ~ 1050 3450
Connection ~ 1050 3550
Connection ~ 1050 3650
Connection ~ 1050 3750
Connection ~ 1050 3850
Connection ~ 1050 3950
Connection ~ 1050 4050
Connection ~ 1050 4150
Connection ~ 1050 4250
Connection ~ 1050 4350
Connection ~ 1050 4450
Connection ~ 1050 4550
Connection ~ 1050 4950
Connection ~ 1050 5050
Connection ~ 1050 5150
Connection ~ 1050 5250
Connection ~ 1050 5350
Connection ~ 1050 5450
Connection ~ 850  4850
Connection ~ 1050 4850
Connection ~ 850  4550
Connection ~ 6800 5500
Connection ~ 1650 7300
Connection ~ 1550 7200
NoConn ~ 10300 3700
NoConn ~ 10300 3800
NoConn ~ 9300 3700
NoConn ~ 9300 3800
Entry Wire Line
	700  7000 800  7100
Entry Wire Line
	700  7100 800  7200
Entry Wire Line
	700  7200 800  7300
Entry Wire Line
	700  7300 800  7400
Entry Wire Line
	800  1100 900  1000
Entry Wire Line
	800  1200 900  1100
Entry Wire Line
	800  1300 900  1200
Entry Wire Line
	800  1400 900  1300
Entry Wire Line
	1950 7700 2050 7600
Entry Wire Line
	2050 7700 2150 7600
Entry Wire Line
	2100 900  2000 800 
Entry Wire Line
	2150 7700 2250 7600
Entry Wire Line
	2200 900  2100 800 
Entry Wire Line
	2250 7700 2350 7600
Entry Wire Line
	2300 900  2200 800 
Entry Wire Line
	2350 7700 2450 7600
Entry Wire Line
	2400 900  2300 800 
Entry Wire Line
	2450 7700 2550 7600
Entry Wire Line
	2500 900  2400 800 
Entry Wire Line
	2550 7700 2650 7600
Entry Wire Line
	2600 900  2500 800 
Entry Wire Line
	2650 7700 2750 7600
Entry Wire Line
	2700 900  2600 800 
Entry Wire Line
	2800 900  2700 800 
Entry Wire Line
	2900 900  2800 800 
Entry Wire Line
	3000 900  2900 800 
Entry Wire Line
	3050 7600 2950 7500
Entry Wire Line
	3100 900  3000 800 
Entry Wire Line
	3150 7600 3050 7500
Entry Wire Line
	3200 900  3100 800 
Entry Wire Line
	3250 7600 3150 7500
Entry Wire Line
	3300 900  3200 800 
Entry Wire Line
	3350 7600 3250 7500
Entry Wire Line
	3400 900  3300 800 
Entry Wire Line
	3450 7600 3350 7500
Entry Wire Line
	3500 900  3400 800 
Entry Wire Line
	3550 7600 3450 7500
Entry Wire Line
	3600 900  3500 800 
Entry Wire Line
	3650 7600 3550 7500
Entry Wire Line
	3700 900  3600 800 
Entry Wire Line
	3750 7600 3650 7500
Entry Wire Line
	3800 900  3700 800 
Entry Wire Line
	3850 7600 3750 7500
Entry Wire Line
	3900 900  3800 800 
Entry Wire Line
	3950 7600 3850 7500
Entry Wire Line
	4000 900  3900 800 
Entry Wire Line
	4050 7600 3950 7500
Entry Wire Line
	4100 900  4000 800 
Entry Wire Line
	4150 7600 4050 7500
Entry Wire Line
	4200 900  4100 800 
Entry Wire Line
	4250 7600 4150 7500
Entry Wire Line
	4300 900  4200 800 
Entry Wire Line
	4400 900  4300 800 
Entry Wire Line
	4400 7600 4300 7500
Entry Wire Line
	4500 7600 4400 7500
Entry Wire Line
	4600 900  4700 800 
Entry Wire Line
	4600 7600 4500 7500
Entry Wire Line
	4700 900  4800 800 
Entry Wire Line
	4700 7600 4600 7500
Entry Wire Line
	4800 900  4900 800 
Entry Wire Line
	4800 7600 4700 7500
Entry Wire Line
	4900 900  5000 800 
Entry Wire Line
	4900 7600 4800 7500
Entry Wire Line
	5000 900  5100 800 
Entry Wire Line
	5000 7600 4900 7500
Entry Wire Line
	5100 900  5200 800 
Entry Wire Line
	5100 7600 5000 7500
Entry Wire Line
	5200 900  5300 800 
Entry Wire Line
	5200 7600 5100 7500
Entry Wire Line
	5300 900  5400 800 
Entry Wire Line
	5300 7600 5200 7500
Entry Wire Line
	5400 900  5500 800 
Entry Wire Line
	5400 7600 5300 7500
Entry Wire Line
	5500 900  5600 800 
Entry Wire Line
	5500 7600 5400 7500
Entry Wire Line
	5600 900  5700 800 
Entry Wire Line
	5600 7600 5500 7500
Entry Wire Line
	5700 900  5800 800 
Entry Wire Line
	5700 7600 5600 7500
Entry Wire Line
	5800 900  5900 800 
Entry Wire Line
	5800 7600 5700 7500
Entry Wire Line
	5900 900  6000 800 
Entry Wire Line
	5900 7600 5800 7500
Entry Wire Line
	6000 900  6100 800 
Entry Wire Line
	6100 900  6200 800 
Entry Wire Line
	7100 2350 7000 2450
Entry Wire Line
	7100 2450 7000 2550
Entry Wire Line
	7100 2550 7000 2650
Entry Wire Line
	7100 2650 7000 2750
Entry Wire Line
	7100 2750 7000 2850
Entry Wire Line
	7100 2850 7000 2950
Entry Wire Line
	7100 2950 7000 3050
Entry Wire Line
	7100 3050 7000 3150
Entry Wire Line
	7100 3150 7000 3250
Entry Wire Line
	7300 3800 7200 3900
Entry Wire Line
	7300 3900 7200 4000
Entry Wire Line
	7300 4000 7200 4100
Entry Wire Line
	7300 4100 7200 4200
Entry Wire Line
	7300 4200 7200 4300
Entry Wire Line
	7300 4300 7200 4400
Entry Wire Line
	8650 3800 8750 3900
Entry Wire Line
	8650 3900 8750 4000
Entry Wire Line
	8650 4000 8750 4100
Entry Wire Line
	8650 4100 8750 4200
Entry Wire Line
	8650 4200 8750 4300
Entry Wire Line
	11050 3900 11150 4000
Entry Wire Line
	11050 4000 11150 4100
Entry Wire Line
	11050 4100 11150 4200
Entry Wire Line
	11050 4200 11150 4300
Entry Wire Line
	11050 4450 11150 4550
Wire Wire Line
	600  4850 850  4850
Wire Wire Line
	700  3050 1050 3050
Wire Wire Line
	700  3850 1050 3850
Wire Wire Line
	800  7100 1450 7100
Wire Wire Line
	800  7200 1550 7200
Wire Wire Line
	800  7300 1650 7300
Wire Wire Line
	800  7400 1750 7400
Wire Wire Line
	850  4550 600  4550
Wire Wire Line
	850  4550 1050 4550
Wire Wire Line
	850  4600 850  4550
Wire Wire Line
	850  4850 850  4800
Wire Wire Line
	850  4850 1050 4850
Wire Wire Line
	900  1000 1850 1000
Wire Wire Line
	900  1100 1750 1100
Wire Wire Line
	900  1200 1650 1200
Wire Wire Line
	900  1300 1550 1300
Wire Wire Line
	950  7450 1550 7450
Wire Wire Line
	950  7550 950  7450
Wire Wire Line
	1050 2850 1050 2950
Wire Wire Line
	1050 2950 1050 3050
Wire Wire Line
	1050 3050 1050 3150
Wire Wire Line
	1050 3350 1050 3450
Wire Wire Line
	1050 3450 1050 3550
Wire Wire Line
	1050 3550 1050 3650
Wire Wire Line
	1050 3650 1050 3750
Wire Wire Line
	1050 3750 1050 3850
Wire Wire Line
	1050 3850 1050 3950
Wire Wire Line
	1050 3950 1050 4050
Wire Wire Line
	1050 4050 1050 4150
Wire Wire Line
	1050 4150 1050 4250
Wire Wire Line
	1050 4250 1050 4350
Wire Wire Line
	1050 4350 1050 4450
Wire Wire Line
	1050 4450 1050 4550
Wire Wire Line
	1050 4550 1050 4650
Wire Wire Line
	1050 4850 1050 4950
Wire Wire Line
	1050 4950 1050 5050
Wire Wire Line
	1050 5050 1050 5150
Wire Wire Line
	1050 5150 700  5150
Wire Wire Line
	1050 5150 1050 5250
Wire Wire Line
	1050 5250 1050 5350
Wire Wire Line
	1050 5350 1050 5450
Wire Wire Line
	1050 5450 1050 5550
Wire Wire Line
	1450 7100 1450 7000
Wire Wire Line
	1550 1300 1550 1350
Wire Wire Line
	1550 7000 1550 7200
Wire Wire Line
	1550 7450 1550 7200
Wire Wire Line
	1650 1200 1650 1350
Wire Wire Line
	1650 7300 1650 7000
Wire Wire Line
	1650 7550 1650 7300
Wire Wire Line
	1750 1100 1750 1350
Wire Wire Line
	1750 7000 1750 7100
Wire Wire Line
	1750 7100 1750 7400
Wire Wire Line
	1750 7100 1850 7100
Wire Wire Line
	1850 1000 1850 1350
Wire Wire Line
	1850 7100 1850 7000
Wire Wire Line
	2050 7600 2050 7000
Wire Wire Line
	2100 1350 2100 900 
Wire Wire Line
	2150 7600 2150 7000
Wire Wire Line
	2200 1350 2200 900 
Wire Wire Line
	2250 7600 2250 7000
Wire Wire Line
	2300 1350 2300 900 
Wire Wire Line
	2350 7600 2350 7000
Wire Wire Line
	2400 1350 2400 900 
Wire Wire Line
	2450 7600 2450 7000
Wire Wire Line
	2500 1350 2500 900 
Wire Wire Line
	2550 7600 2550 7000
Wire Wire Line
	2600 1350 2600 900 
Wire Wire Line
	2650 7600 2650 7000
Wire Wire Line
	2700 1350 2700 900 
Wire Wire Line
	2750 7600 2750 7000
Wire Wire Line
	2800 1350 2800 900 
Wire Wire Line
	2900 1350 2900 900 
Wire Wire Line
	2950 7500 2950 7000
Wire Wire Line
	3000 1350 3000 900 
Wire Wire Line
	3050 7500 3050 7000
Wire Wire Line
	3100 1350 3100 900 
Wire Wire Line
	3150 7500 3150 7000
Wire Wire Line
	3200 1350 3200 900 
Wire Wire Line
	3250 7500 3250 7000
Wire Wire Line
	3300 1350 3300 900 
Wire Wire Line
	3350 7500 3350 7000
Wire Wire Line
	3400 1350 3400 900 
Wire Wire Line
	3450 7500 3450 7000
Wire Wire Line
	3500 1350 3500 900 
Wire Wire Line
	3550 7500 3550 7000
Wire Wire Line
	3600 1350 3600 900 
Wire Wire Line
	3650 7500 3650 7000
Wire Wire Line
	3700 1350 3700 900 
Wire Wire Line
	3750 7500 3750 7000
Wire Wire Line
	3800 1350 3800 900 
Wire Wire Line
	3850 7500 3850 7000
Wire Wire Line
	3900 1350 3900 900 
Wire Wire Line
	3950 7500 3950 7000
Wire Wire Line
	4000 1350 4000 900 
Wire Wire Line
	4050 7500 4050 7000
Wire Wire Line
	4100 1350 4100 900 
Wire Wire Line
	4150 7500 4150 7000
Wire Wire Line
	4200 1350 4200 900 
Wire Wire Line
	4300 1350 4300 900 
Wire Wire Line
	4300 7500 4300 7000
Wire Wire Line
	4400 1350 4400 900 
Wire Wire Line
	4400 7500 4400 7000
Wire Wire Line
	4500 7500 4500 7000
Wire Wire Line
	4600 1350 4600 900 
Wire Wire Line
	4600 7500 4600 7000
Wire Wire Line
	4700 1350 4700 900 
Wire Wire Line
	4700 7500 4700 7000
Wire Wire Line
	4800 1350 4800 900 
Wire Wire Line
	4800 7500 4800 7000
Wire Wire Line
	4900 1350 4900 900 
Wire Wire Line
	4900 7500 4900 7000
Wire Wire Line
	5000 1350 5000 900 
Wire Wire Line
	5000 7500 5000 7000
Wire Wire Line
	5100 1350 5100 900 
Wire Wire Line
	5100 7500 5100 7000
Wire Wire Line
	5200 1350 5200 900 
Wire Wire Line
	5200 7500 5200 7000
Wire Wire Line
	5300 1350 5300 900 
Wire Wire Line
	5300 7500 5300 7000
Wire Wire Line
	5400 1350 5400 900 
Wire Wire Line
	5400 7500 5400 7000
Wire Wire Line
	5500 1350 5500 900 
Wire Wire Line
	5500 7500 5500 7000
Wire Wire Line
	5600 1350 5600 900 
Wire Wire Line
	5600 7500 5600 7000
Wire Wire Line
	5700 1350 5700 900 
Wire Wire Line
	5700 7500 5700 7000
Wire Wire Line
	5800 1350 5800 900 
Wire Wire Line
	5800 7500 5800 7000
Wire Wire Line
	5900 1350 5900 900 
Wire Wire Line
	6000 1350 6000 900 
Wire Wire Line
	6000 7000 6000 7100
Wire Wire Line
	6100 1350 6100 900 
Wire Wire Line
	6100 7000 6100 7100
Wire Wire Line
	6200 7000 6200 7100
Wire Wire Line
	6300 7000 6300 7100
Wire Wire Line
	6700 2450 7000 2450
Wire Wire Line
	6700 2550 7000 2550
Wire Wire Line
	6700 2650 7000 2650
Wire Wire Line
	6700 2750 7000 2750
Wire Wire Line
	6700 2850 7000 2850
Wire Wire Line
	6700 2950 7000 2950
Wire Wire Line
	6700 3050 7000 3050
Wire Wire Line
	6700 3150 7000 3150
Wire Wire Line
	6700 3250 7000 3250
Wire Wire Line
	6700 3550 7150 3550
Wire Wire Line
	6700 3650 7150 3650
Wire Wire Line
	6700 3900 7200 3900
Wire Wire Line
	6700 4000 7200 4000
Wire Wire Line
	6700 4100 7200 4100
Wire Wire Line
	6700 4200 7200 4200
Wire Wire Line
	6700 4300 7200 4300
Wire Wire Line
	6700 4400 7200 4400
Wire Wire Line
	6750 3450 6700 3450
Wire Wire Line
	6800 5500 6700 5500
Wire Wire Line
	6800 5550 6800 5500
Wire Wire Line
	6800 5800 6700 5800
Wire Wire Line
	6800 5850 6800 5800
Wire Wire Line
	6900 2300 6700 2300
Wire Wire Line
	6900 4600 6700 4600
Wire Wire Line
	6900 4750 6700 4750
Wire Wire Line
	6900 4900 6700 4900
Wire Wire Line
	6900 5100 6700 5100
Wire Wire Line
	6900 5200 6700 5200
Wire Wire Line
	6900 5400 6700 5400
Wire Wire Line
	6900 5500 6800 5500
Wire Wire Line
	6900 5650 6700 5650
Wire Wire Line
	6900 5800 6800 5800
Wire Wire Line
	6900 5950 6700 5950
Wire Wire Line
	6900 6050 6700 6050
Wire Wire Line
	7150 3450 6950 3450
Wire Wire Line
	7400 5550 6800 5550
Wire Wire Line
	7400 5850 6800 5850
Wire Wire Line
	7700 5550 7600 5550
Wire Wire Line
	7700 5850 7600 5850
Wire Wire Line
	8750 3900 9300 3900
Wire Wire Line
	8750 4000 8800 4000
Wire Wire Line
	8750 4100 9300 4100
Wire Wire Line
	8750 4200 9300 4200
Wire Wire Line
	8750 4300 9300 4300
Wire Wire Line
	8800 3700 8800 4000
Wire Wire Line
	8800 4000 9300 4000
Wire Wire Line
	8850 3700 8800 3700
Wire Wire Line
	9300 4300 9300 4400
Wire Wire Line
	9300 4600 9200 4600
Wire Wire Line
	9300 5000 9300 4700
Wire Wire Line
	9500 3400 9800 3400
Wire Wire Line
	9800 5000 9300 5000
Wire Wire Line
	10150 3400 9800 3400
Wire Wire Line
	10300 3900 11050 3900
Wire Wire Line
	10300 4000 11050 4000
Wire Wire Line
	10300 4100 11050 4100
Wire Wire Line
	10300 4200 11050 4200
Wire Wire Line
	10300 4300 10450 4300
Wire Wire Line
	10300 4400 10400 4400
Wire Wire Line
	10400 4400 10400 4450
Wire Wire Line
	10400 4450 11050 4450
Wire Bus Line
	700  6900 700  7100
Wire Bus Line
	700  7100 700  7300
Wire Bus Line
	800  1100 800  1300
Wire Bus Line
	800  1300 800  1500
Wire Bus Line
	1650 800  2100 800 
Wire Bus Line
	1800 7700 1950 7700
Wire Bus Line
	1950 7700 2150 7700
Wire Bus Line
	2100 800  2200 800 
Wire Bus Line
	2150 7700 2350 7700
Wire Bus Line
	2200 800  2400 800 
Wire Bus Line
	2350 7700 2550 7700
Wire Bus Line
	2400 800  2600 800 
Wire Bus Line
	2550 7700 2800 7700
Wire Bus Line
	2600 800  2800 800 
Wire Bus Line
	2800 800  3000 800 
Wire Bus Line
	3000 800  3200 800 
Wire Bus Line
	3050 7600 3250 7600
Wire Bus Line
	3200 800  3400 800 
Wire Bus Line
	3250 7600 3450 7600
Wire Bus Line
	3400 800  3600 800 
Wire Bus Line
	3450 7600 3650 7600
Wire Bus Line
	3600 800  3800 800 
Wire Bus Line
	3650 7600 3850 7600
Wire Bus Line
	3800 800  4000 800 
Wire Bus Line
	3850 7600 4050 7600
Wire Bus Line
	4000 800  4100 800 
Wire Bus Line
	4050 7600 4250 7600
Wire Bus Line
	4100 800  4300 800 
Wire Bus Line
	4200 7700 4300 7700
Wire Bus Line
	4250 7600 4300 7600
Wire Bus Line
	4300 7700 4300 7600
Wire Bus Line
	4400 7600 4600 7600
Wire Bus Line
	4600 7600 4800 7600
Wire Bus Line
	4700 800  4900 800 
Wire Bus Line
	4800 7600 5000 7600
Wire Bus Line
	4900 800  5100 800 
Wire Bus Line
	5000 7600 5100 7600
Wire Bus Line
	5100 800  5300 800 
Wire Bus Line
	5100 7600 5300 7600
Wire Bus Line
	5300 800  5500 800 
Wire Bus Line
	5300 7600 5500 7600
Wire Bus Line
	5500 800  5700 800 
Wire Bus Line
	5500 7600 5700 7600
Wire Bus Line
	5700 800  5900 800 
Wire Bus Line
	5700 7600 5800 7600
Wire Bus Line
	5800 7600 5950 7600
Wire Bus Line
	5850 7700 5950 7700
Wire Bus Line
	5900 800  6100 800 
Wire Bus Line
	5950 7600 5950 7700
Wire Bus Line
	6100 800  6300 800 
Wire Bus Line
	7100 2350 7100 2450
Wire Bus Line
	7100 2350 7450 2350
Wire Bus Line
	7100 2450 7100 2650
Wire Bus Line
	7100 2650 7100 2850
Wire Bus Line
	7100 2850 7100 3050
Wire Bus Line
	7100 3050 7100 3150
Wire Bus Line
	7300 3800 7300 4000
Wire Bus Line
	7300 3800 7650 3800
Wire Bus Line
	7300 4000 7300 4200
Wire Bus Line
	7300 4200 7300 4300
Wire Bus Line
	7650 3800 7650 4000
Wire Bus Line
	7650 3800 8650 3800
Wire Bus Line
	7650 4000 7750 4000
Wire Bus Line
	8650 3800 8650 4000
Wire Bus Line
	8650 4000 8650 4200
Wire Bus Line
	11150 4000 11150 4200
Wire Bus Line
	11150 4200 11150 4550
Wire Bus Line
	11150 4550 11150 4600
Wire Bus Line
	11150 4600 11000 4600
Text Label 850  7100 0    50   ~ 0
SECURITY-IN2
Text Label 850  7200 0    50   ~ 0
SECURITY-IN3
Text Label 850  7300 0    50   ~ 0
SECURITY-IN4
Text Label 850  7400 0    50   ~ 0
SECURITY-IN5
Text Label 950  1000 0    50   ~ 0
SECURITY-OUT5
Text Label 950  1100 0    50   ~ 0
SECURITY-OUT4
Text Label 950  1200 0    50   ~ 0
SECURITY-OUT3
Text Label 950  1300 0    50   ~ 0
SECURITY-OUT2
Text Label 2050 7550 1    50   ~ 0
CPU-ADDR16
Text Label 2100 1300 1    50   ~ 0
CPU-A0
Text Label 2150 7550 1    50   ~ 0
CPU-ADDR17
Text Label 2200 1300 1    50   ~ 0
CPU-A1
Text Label 2250 7550 1    50   ~ 0
CPU-ADDR18
Text Label 2300 1300 1    50   ~ 0
CPU-A2
Text Label 2350 7550 1    50   ~ 0
CPU-ADDR19
Text Label 2400 1300 1    50   ~ 0
CPU-A3
Text Label 2450 7550 1    50   ~ 0
CPU-ADDR20
Text Label 2500 1300 1    50   ~ 0
CPU-A4
Text Label 2550 7550 1    50   ~ 0
CPU-ADDR21
Text Label 2600 1300 1    50   ~ 0
CPU-A5
Text Label 2650 7550 1    50   ~ 0
CPU-ADDR22
Text Label 2700 1300 1    50   ~ 0
CPU-A6
Text Label 2750 7550 1    50   ~ 0
CPU-ADDR23
Text Label 2800 1300 1    50   ~ 0
CPU-A7
Text Label 2800 7700 0    50   ~ 0
CPU-ADDR[16..23]
Text Label 2900 1300 1    50   ~ 0
CPU-A8
Text Label 2950 7450 1    50   ~ 0
SRAM-A0
Text Label 3000 1300 1    50   ~ 0
CPU-A9
Text Label 3050 7450 1    50   ~ 0
SRAM-A1
Text Label 3100 1300 1    50   ~ 0
CPU-A10
Text Label 3150 7450 1    50   ~ 0
SRAM-A2
Text Label 3200 1300 1    50   ~ 0
CPU-A11
Text Label 3250 7450 1    50   ~ 0
SRAM-A3
Text Label 3300 1300 1    50   ~ 0
CPU-A12
Text Label 3350 7450 1    50   ~ 0
SRAM-A4
Text Label 3400 1300 1    50   ~ 0
CPU-A13
Text Label 3450 7450 1    50   ~ 0
SRAM-A5
Text Label 3500 1300 1    50   ~ 0
CPU-A14
Text Label 3550 7450 1    50   ~ 0
SRAM-A6
Text Label 3600 1300 1    50   ~ 0
CPU-A15
Text Label 3650 7450 1    50   ~ 0
SRAM-A7
Text Label 3700 1300 1    50   ~ 0
CPU-A16
Text Label 3750 7450 1    50   ~ 0
SRAM-A8
Text Label 3800 1300 1    50   ~ 0
CPU-A17
Text Label 3850 7450 1    50   ~ 0
SRAM-A9
Text Label 3900 1300 1    50   ~ 0
CPU-A18
Text Label 3950 7450 1    50   ~ 0
SRAM-A10
Text Label 4000 1300 1    50   ~ 0
CPU-A19
Text Label 4050 7450 1    50   ~ 0
SRAM-A11
Text Label 4100 1300 1    50   ~ 0
CPU-A20
Text Label 4150 7450 1    50   ~ 0
SRAM-A12
Text Label 4200 1300 1    50   ~ 0
CPU-A21
Text Label 4300 1300 1    50   ~ 0
CPU-A22
Text Label 4300 7450 1    50   ~ 0
SRAM-D0
Text Label 4400 1300 1    50   ~ 0
CPU-A23
Text Label 4400 7450 1    50   ~ 0
SRAM-D1
Text Label 4500 7450 1    50   ~ 0
SRAM-D2
Text Label 4600 1300 1    50   ~ 0
CPU-D0
Text Label 4600 7450 1    50   ~ 0
SRAM-D3
Text Label 4700 1300 1    50   ~ 0
CPU-D1
Text Label 4700 7450 1    50   ~ 0
SRAM-D4
Text Label 4800 1300 1    50   ~ 0
CPU-D2
Text Label 4800 7450 1    50   ~ 0
SRAM-D5
Text Label 4900 1300 1    50   ~ 0
CPU-D3
Text Label 4900 7450 1    50   ~ 0
SRAM-D6
Text Label 5000 1300 1    50   ~ 0
CPU-D4
Text Label 5000 7450 1    50   ~ 0
SRAM-D7
Text Label 5100 1300 1    50   ~ 0
CPU-D5
Text Label 5100 7450 1    50   ~ 0
SRAM-D8
Text Label 5200 1300 1    50   ~ 0
CPU-D6
Text Label 5200 7450 1    50   ~ 0
SRAM-D9
Text Label 5300 1300 1    50   ~ 0
CPU-D7
Text Label 5300 7450 1    50   ~ 0
SRAM-D10
Text Label 5400 1300 1    50   ~ 0
CPU-D8
Text Label 5400 7450 1    50   ~ 0
SRAM-D11
Text Label 5500 1300 1    50   ~ 0
CPU-D9
Text Label 5500 7450 1    50   ~ 0
SRAM-D12
Text Label 5600 1300 1    50   ~ 0
CPU-D10
Text Label 5600 7450 1    50   ~ 0
SRAM-D13
Text Label 5700 1300 1    50   ~ 0
CPU-D11
Text Label 5700 7450 1    50   ~ 0
SRAM-D14
Text Label 5800 1300 1    50   ~ 0
CPU-D12
Text Label 5800 7450 1    50   ~ 0
SRAM-D15
Text Label 5900 1300 1    50   ~ 0
CPU-D13
Text Label 6000 1300 1    50   ~ 0
CPU-D14
Text Label 6100 1300 1    50   ~ 0
CPU-D15
Text Label 6750 2450 0    50   ~ 0
UK26
Text Label 6750 2550 0    50   ~ 0
UK27
Text Label 6750 2650 0    50   ~ 0
UK28
Text Label 6750 2750 0    50   ~ 0
UK29
Text Label 6750 2850 0    50   ~ 0
UK31
Text Label 6750 2950 0    50   ~ 0
UK32
Text Label 6750 3050 0    50   ~ 0
UK33
Text Label 6750 3150 0    50   ~ 0
UK34
Text Label 6750 3250 0    50   ~ 0
UK35
Text Label 6750 3900 0    50   ~ 0
CONTROL1
Text Label 6750 4000 0    50   ~ 0
CONTROL2
Text Label 6750 4100 0    50   ~ 0
CONTROL3
Text Label 6750 4200 0    50   ~ 0
CONTROL4
Text Label 6750 4300 0    50   ~ 0
CONTROL5
Text Label 6750 4400 0    50   ~ 0
CONTROL6
Text Label 8850 3900 0    50   ~ 0
CONTROL2
Text Label 8850 4000 0    50   ~ 0
CONTROL4
Text Label 8850 4100 0    50   ~ 0
CONTROL1
Text Label 8850 4200 0    50   ~ 0
CONTROL6
Text Label 8850 4300 0    50   ~ 0
CONTROL3
Text Label 10450 3900 0    50   ~ 0
CONTROL-OUT2
Text Label 10450 4000 0    50   ~ 0
CONTROL-OUT4
Text Label 10450 4100 0    50   ~ 0
CONTROL-OUT1
Text Label 10450 4200 0    50   ~ 0
CONTROL-OUT6
Text Label 10450 4450 0    50   ~ 0
CONTROL-OUT3
Text HLabel 700  6900 1    50   Input ~ 0
SECURITY-IN[2..5]
Text HLabel 800  1500 3    50   Input ~ 0
SECURITY-OUT[2..5]
Text HLabel 1650 800  0    50   Input ~ 0
CPU-A[0..23]
Text HLabel 1800 7700 0    50   Input ~ 0
CODE-ADDR[16..23]
Text HLabel 4200 7700 0    50   Output ~ 0
SRAM-A[0..12]
Text HLabel 5850 7700 0    50   BiDi ~ 0
SRAM-D[0..15]
Text HLabel 6000 7100 3    50   Output ~ 0
~SRAM-WH
Text HLabel 6100 7100 3    50   Output ~ 0
~SRAM-WL
Text HLabel 6200 7100 3    50   Output ~ 0
~SRAM-OE
Text HLabel 6300 800  2    50   BiDi ~ 0
CPU-D[0..15]
Text HLabel 6300 7100 3    50   Output ~ 0
~SRAM-E
Text HLabel 6900 2300 2    50   BiDi ~ 0
UK22
Text HLabel 6900 4600 2    50   Input ~ 0
ENABLE3
Text HLabel 6900 4750 2    50   Input ~ 0
ROM-DATA0
Text HLabel 6900 4900 2    50   BiDi ~ 0
UK103
Text HLabel 6900 5100 2    50   BiDi ~ 0
UK126
Text HLabel 6900 5200 2    50   BiDi ~ 0
UK133
Text HLabel 6900 5400 2    50   Input ~ 0
~VBLANK
Text HLabel 6900 5500 2    50   BiDi ~ 0
UK153
Text HLabel 6900 5650 2    50   Input ~ 0
ENABLE
Text HLabel 6900 5800 2    50   BiDi ~ 0
UK155
Text HLabel 6900 5950 2    50   BiDi ~ 0
UK157
Text HLabel 6900 6050 2    50   BiDi ~ 0
UK158
Text HLabel 7150 3450 2    50   Input ~ 0
CLK16M
Text HLabel 7150 3550 2    50   Input ~ 0
CLK4M
Text HLabel 7150 3650 2    50   Output ~ 0
CLK-OUT
Text HLabel 7450 2350 2    50   BiDi ~ 0
UK[26..35]
Text HLabel 7750 4000 2    50   BiDi ~ 0
CONTROL[1..6]
Text HLabel 8850 3700 2    50   Output ~ 0
~ROM-E
Text HLabel 10450 4300 2    50   Output ~ 0
~ROM-OE
Text HLabel 11000 4600 0    50   Output ~ 0
CONTROL-OUT[1..6]
$Comp
L power:VDD #PWR018
U 1 1 5E63F941
P 700 5150
F 0 "#PWR018" H 700 5000 50  0001 C CNN
F 1 "VDD" H 717 5323 50  0000 C CNN
F 2 "" H 700 5150 50  0001 C CNN
F 3 "" H 700 5150 50  0001 C CNN
	1    700  5150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E5504EE
P 7700 5550
AR Path="/635BEEB0/5E5504EE" Ref="#PWR?"  Part="1" 
AR Path="/5E5504EE" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E5504EE" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E5504EE" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E5504EE" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 7700 5400 50  0001 C CNN
F 1 "VCC" H 7500 5650 50  0000 L CNN
F 2 "" H 7700 5550 50  0001 C CNN
F 3 "" H 7700 5550 50  0001 C CNN
	1    7700 5550
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E5433C6
P 7700 5850
AR Path="/635BEEB0/5E5433C6" Ref="#PWR?"  Part="1" 
AR Path="/5E5433C6" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E5433C6" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E5433C6" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E5433C6" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 7700 5700 50  0001 C CNN
F 1 "VCC" H 7500 5950 50  0000 L CNN
F 2 "" H 7700 5850 50  0001 C CNN
F 3 "" H 7700 5850 50  0001 C CNN
	1    7700 5850
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E362A1C
P 9200 4600
AR Path="/635BEEB0/5E362A1C" Ref="#PWR?"  Part="1" 
AR Path="/5E362A1C" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E362A1C" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E362A1C" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E362A1C" Ref="#PWR0277"  Part="1" 
F 0 "#PWR0277" H 9200 4450 50  0001 C CNN
F 1 "VCC" H 9218 4728 50  0000 L CNN
F 2 "" H 9200 4600 50  0001 C CNN
F 3 "" H 9200 4600 50  0001 C CNN
	1    9200 4600
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E3629F8
P 10150 3400
AR Path="/635BEEB0/5E3629F8" Ref="#PWR?"  Part="1" 
AR Path="/5E3629F8" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E3629F8" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E3629F8" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E3629F8" Ref="#PWR0275"  Part="1" 
F 0 "#PWR0275" H 10150 3250 50  0001 C CNN
F 1 "VCC" H 9950 3500 50  0000 L CNN
F 2 "" H 10150 3400 50  0001 C CNN
F 3 "" H 10150 3400 50  0001 C CNN
	1    10150 3400
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5E6B565A
P 700 3050
F 0 "#PWR016" H 700 2800 50  0001 C CNN
F 1 "GND" H 705 2877 50  0001 C CNN
F 2 "" H 700 3050 50  0001 C CNN
F 3 "" H 700 3050 50  0001 C CNN
	1    700  3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5E63F900
P 700 3850
F 0 "#PWR017" H 700 3600 50  0001 C CNN
F 1 "GND" H 705 3677 50  0001 C CNN
F 2 "" H 700 3850 50  0001 C CNN
F 3 "" H 700 3850 50  0001 C CNN
	1    700  3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E97305E
P 750 7550
AR Path="/635BEEB0/5E97305E" Ref="#PWR?"  Part="1" 
AR Path="/5E97305E" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E97305E" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E97305E" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E97305E" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 750 7300 50  0001 C CNN
F 1 "GND" H 755 7377 50  0001 C CNN
F 2 "" H 750 7550 50  0001 C CNN
F 3 "" H 750 7550 50  0001 C CNN
	1    750  7550
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E5861D8
P 1450 7550
AR Path="/635BEEB0/5E5861D8" Ref="#PWR?"  Part="1" 
AR Path="/5E5861D8" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E5861D8" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E5861D8" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E5861D8" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 1450 7300 50  0001 C CNN
F 1 "GND" H 1455 7377 50  0001 C CNN
F 2 "" H 1450 7550 50  0001 C CNN
F 3 "" H 1450 7550 50  0001 C CNN
	1    1450 7550
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E362A0A
P 9300 3400
AR Path="/635BEEB0/5E362A0A" Ref="#PWR?"  Part="1" 
AR Path="/5E362A0A" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E362A0A" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E362A0A" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E362A0A" Ref="#PWR0276"  Part="1" 
F 0 "#PWR0276" H 9300 3150 50  0001 C CNN
F 1 "GND" H 9305 3227 50  0001 C CNN
F 2 "" H 9300 3400 50  0001 C CNN
F 3 "" H 9300 3400 50  0001 C CNN
	1    9300 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E3629F2
P 9800 5000
AR Path="/635BEEB0/5E3629F2" Ref="#PWR?"  Part="1" 
AR Path="/5E3629F2" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/5E3629F2" Ref="#PWR?"  Part="1" 
AR Path="/5E1CA863/5E3629F2" Ref="#PWR?"  Part="1" 
AR Path="/5E35B989/5E3629F2" Ref="#PWR0274"  Part="1" 
F 0 "#PWR0274" H 9800 4750 50  0001 C CNN
F 1 "GND" H 9805 4827 50  0001 C CNN
F 2 "" H 9800 5000 50  0001 C CNN
F 3 "" H 9800 5000 50  0001 C CNN
	1    9800 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E973057
P 850 7550
AR Path="/5D9F70E5/5E973057" Ref="R?"  Part="1" 
AR Path="/5E35B989/5E973057" Ref="R2"  Part="1" 
F 0 "R2" V 750 7750 50  0000 C CNN
F 1 "4.7k" V 850 7750 50  0000 C CNN
F 2 "" H 850 7550 50  0001 C CNN
F 3 "~" H 850 7550 50  0001 C CNN
	1    850  7550
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E5861D1
P 1550 7550
AR Path="/5D9F70E5/5E5861D1" Ref="R?"  Part="1" 
AR Path="/5E35B989/5E5861D1" Ref="R6"  Part="1" 
F 0 "R6" V 1450 7350 50  0000 C CNN
F 1 "4.7k" V 1550 7350 50  0000 C CNN
F 2 "" H 1550 7550 50  0001 C CNN
F 3 "~" H 1550 7550 50  0001 C CNN
	1    1550 7550
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E5504E6
P 7500 5550
AR Path="/5D9F70E5/5E5504E6" Ref="R?"  Part="1" 
AR Path="/5E35B989/5E5504E6" Ref="R11"  Part="1" 
F 0 "R11" V 7300 5550 50  0000 C CNN
F 1 "4.7k" V 7400 5550 50  0000 C CNN
F 2 "" H 7500 5550 50  0001 C CNN
F 3 "~" H 7500 5550 50  0001 C CNN
	1    7500 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E51192D
P 7500 5850
AR Path="/5D9F70E5/5E51192D" Ref="R?"  Part="1" 
AR Path="/5E35B989/5E51192D" Ref="R1"  Part="1" 
F 0 "R1" V 7300 5850 50  0000 C CNN
F 1 "4.7k" V 7400 5850 50  0000 C CNN
F 2 "" H 7500 5850 50  0001 C CNN
F 3 "~" H 7500 5850 50  0001 C CNN
	1    7500 5850
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX33
U 1 1 5E941052
P 850 4700
F 0 "CX33" H 942 4746 50  0001 L CNN
F 1 "0.1u" H 650 4600 50  0000 L CNN
F 2 "" H 850 4700 50  0001 C CNN
F 3 "~" H 850 4700 50  0001 C CNN
	1    850  4700
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E362A04
P 9400 3400
AR Path="/635BEEB0/5E362A04" Ref="C?"  Part="1" 
AR Path="/5E362A04" Ref="C?"  Part="1" 
AR Path="/5DC1E7F0/5E362A04" Ref="C?"  Part="1" 
AR Path="/5E1CA863/5E362A04" Ref="C?"  Part="1" 
AR Path="/5E35B989/5E362A04" Ref="CX34"  Part="1" 
F 0 "CX34" V 9171 3400 50  0001 C CNN
F 1 "0.1u" V 9263 3400 50  0000 C TNN
F 2 "" H 9400 3400 50  0001 C CNN
F 3 "~" H 9400 3400 50  0001 C CNN
	1    9400 3400
	0    1    1    0   
$EndComp
$Comp
L Device:Ferrite_Bead_Small FB1
U 1 1 5DC9AE69
P 6850 3450
F 0 "FB1" V 6995 3450 50  0000 C CNN
F 1 "Ferrite_Bead_Small" V 6996 3450 50  0001 C CNN
F 2 "" V 6780 3450 50  0001 C CNN
F 3 "~" H 6850 3450 50  0001 C CNN
	1    6850 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C EXC1
U 1 1 5DD51758
P 600 4700
F 0 "EXC1" H 600 4800 50  0000 L CNN
F 1 "1u" H 600 4600 50  0000 L CNN
F 2 "" H 638 4550 50  0001 C CNN
F 3 "~" H 600 4700 50  0001 C CNN
	1    600  4700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS245 U10F?
U 1 1 5E362A46
P 9800 4200
AR Path="/635BEEB0/5E362A46" Ref="U10F?"  Part="1" 
AR Path="/5E362A46" Ref="U10F?"  Part="1" 
AR Path="/5DC1E7F0/5E362A46" Ref="U10F?"  Part="1" 
AR Path="/5E1CA863/5E362A46" Ref="U10F?"  Part="1" 
AR Path="/5E35B989/5E362A46" Ref="UF10"  Part="1" 
F 0 "UF10" H 9800 5178 50  0000 C CNN
F 1 "74LS245" H 9800 5087 50  0000 C CNN
F 2 "" H 9800 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 9800 4200 50  0001 C CNN
	1    9800 4200
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:DL-1827 U-CIF-B4
U 1 1 5E35B9B9
P 3850 4200
F 0 "U-CIF-B4" H 3850 4400 200 0000 C CNN
F 1 "DL-1827" H 3850 4150 200 0000 C CNN
F 2 "" H 6950 6100 50  0001 C CNN
F 3 "" H 6950 6100 50  0001 C CNN
	1    3850 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
