EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 17
Title ""
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 2650 2800
Connection ~ 2650 2900
Connection ~ 2650 3000
Connection ~ 2650 3100
Connection ~ 2650 3200
Connection ~ 2650 3300
Connection ~ 2650 3400
Connection ~ 2650 3500
Connection ~ 2650 3600
Connection ~ 2650 3700
Connection ~ 2650 3800
Connection ~ 2650 3900
Connection ~ 2650 4000
Connection ~ 2650 4100
Connection ~ 2650 4200
Connection ~ 2650 4300
Connection ~ 2650 4400
Connection ~ 2650 4500
Connection ~ 2650 4600
Connection ~ 2650 4700
Connection ~ 2650 4800
Connection ~ 2650 4900
Connection ~ 2650 5000
Connection ~ 2650 5100
Connection ~ 2650 5400
Connection ~ 2650 5500
Connection ~ 2650 5600
Connection ~ 2650 5700
Connection ~ 3700 5350
Connection ~ 3700 5450
Connection ~ 3700 5550
Connection ~ 3700 5650
Connection ~ 3700 5750
Connection ~ 3700 5250
Connection ~ 3700 4450
Connection ~ 3700 4550
Connection ~ 3700 4650
Connection ~ 3700 4750
Connection ~ 3700 4850
Connection ~ 3700 4950
Entry Wire Line
	4200 3850 4300 3950
Entry Wire Line
	4200 3950 4300 4050
Entry Wire Line
	4200 4050 4300 4150
Entry Wire Line
	4200 4150 4300 4250
Entry Wire Line
	4250 5950 4350 5850
Entry Wire Line
	4250 6050 4350 5950
Entry Wire Line
	4250 6150 4350 6050
Entry Wire Line
	4250 6250 4350 6150
Entry Wire Line
	4250 6350 4350 6250
Entry Wire Line
	4250 6450 4350 6350
Entry Wire Line
	4250 6550 4350 6450
Entry Wire Line
	4250 6650 4350 6550
Entry Wire Line
	4250 6750 4350 6650
Entry Wire Line
	4250 6850 4350 6750
Entry Wire Line
	4250 6950 4350 6850
Entry Wire Line
	4250 7050 4350 6950
Entry Wire Line
	4250 7150 4350 7050
Entry Wire Line
	4250 7250 4350 7150
Entry Wire Line
	4250 7350 4350 7250
Entry Wire Line
	4250 7450 4350 7350
Entry Wire Line
	4300 1850 4400 1950
Entry Wire Line
	4300 1950 4400 2050
Entry Wire Line
	4300 2050 4400 2150
Entry Wire Line
	4300 2150 4400 2250
Entry Wire Line
	4300 2250 4400 2350
Entry Wire Line
	4300 2350 4400 2450
Entry Wire Line
	4300 2450 4400 2550
Entry Wire Line
	4300 2550 4400 2650
Entry Wire Line
	4300 2650 4400 2750
Entry Wire Line
	4300 2750 4400 2850
Entry Wire Line
	4300 2850 4400 2950
Entry Wire Line
	4300 2950 4400 3050
Entry Wire Line
	4300 3050 4400 3150
Entry Wire Line
	4300 3150 4400 3250
Entry Wire Line
	4300 3250 4400 3350
Entry Wire Line
	4300 3350 4400 3450
Entry Wire Line
	4300 3450 4400 3550
Entry Wire Line
	4300 3550 4400 3650
Entry Wire Line
	4300 3650 4400 3750
Entry Wire Line
	4300 3750 4400 3850
Wire Wire Line
	2500 3800 2650 3800
Wire Wire Line
	2500 5500 2650 5500
Wire Wire Line
	2650 2700 2650 2800
Wire Wire Line
	2650 2800 2650 2900
Wire Wire Line
	2650 2900 2650 3000
Wire Wire Line
	2650 3000 2650 3100
Wire Wire Line
	2650 3100 2650 3200
Wire Wire Line
	2650 3200 2650 3300
Wire Wire Line
	2650 3300 2650 3400
Wire Wire Line
	2650 3400 2650 3500
Wire Wire Line
	2650 3500 2650 3600
Wire Wire Line
	2650 3600 2650 3700
Wire Wire Line
	2650 3700 2650 3800
Wire Wire Line
	2650 3800 2650 3900
Wire Wire Line
	2650 3900 2650 4000
Wire Wire Line
	2650 4000 2650 4100
Wire Wire Line
	2650 4100 2650 4200
Wire Wire Line
	2650 4200 2650 4300
Wire Wire Line
	2650 4300 2650 4400
Wire Wire Line
	2650 4400 2650 4500
Wire Wire Line
	2650 4500 2650 4600
Wire Wire Line
	2650 4600 2650 4700
Wire Wire Line
	2650 4700 2650 4800
Wire Wire Line
	2650 4800 2650 4900
Wire Wire Line
	2650 4900 2650 5000
Wire Wire Line
	2650 5000 2650 5100
Wire Wire Line
	2650 5100 2650 5200
Wire Wire Line
	2650 5300 2650 5400
Wire Wire Line
	2650 5400 2650 5500
Wire Wire Line
	2650 5500 2650 5600
Wire Wire Line
	2650 5600 2650 5700
Wire Wire Line
	2650 5700 2650 5800
Wire Wire Line
	3700 1150 4100 1150
Wire Wire Line
	3700 1250 4100 1250
Wire Wire Line
	3700 1350 4100 1350
Wire Wire Line
	3700 1450 4100 1450
Wire Wire Line
	3700 1550 4100 1550
Wire Wire Line
	3700 1650 4600 1650
Wire Wire Line
	3700 1750 4250 1750
Wire Wire Line
	3700 1850 4300 1850
Wire Wire Line
	3700 1950 4300 1950
Wire Wire Line
	3700 2050 4300 2050
Wire Wire Line
	3700 2150 4300 2150
Wire Wire Line
	3700 2250 4300 2250
Wire Wire Line
	3700 2350 4300 2350
Wire Wire Line
	3700 2450 4300 2450
Wire Wire Line
	3700 2550 4300 2550
Wire Wire Line
	3700 2650 4300 2650
Wire Wire Line
	3700 2750 4300 2750
Wire Wire Line
	3700 2850 4300 2850
Wire Wire Line
	3700 2950 4300 2950
Wire Wire Line
	3700 3050 4300 3050
Wire Wire Line
	3700 3150 4300 3150
Wire Wire Line
	3700 3250 4300 3250
Wire Wire Line
	3700 3350 4300 3350
Wire Wire Line
	3700 3450 4300 3450
Wire Wire Line
	3700 3550 4300 3550
Wire Wire Line
	3700 3650 4300 3650
Wire Wire Line
	3700 3750 4300 3750
Wire Wire Line
	3700 3850 4200 3850
Wire Wire Line
	3700 3950 4200 3950
Wire Wire Line
	3700 4050 4200 4050
Wire Wire Line
	3700 4150 4200 4150
Wire Wire Line
	3700 4450 3700 4350
Wire Wire Line
	3700 4550 3700 4450
Wire Wire Line
	3700 4650 3700 4550
Wire Wire Line
	3700 4750 3700 4650
Wire Wire Line
	3700 4850 3700 4750
Wire Wire Line
	3700 4950 3700 4850
Wire Wire Line
	3700 5050 3700 4950
Wire Wire Line
	3700 5250 3700 5150
Wire Wire Line
	3700 5350 3700 5250
Wire Wire Line
	3700 5450 3700 5350
Wire Wire Line
	3700 5550 3700 5450
Wire Wire Line
	3700 5650 3700 5550
Wire Wire Line
	3700 5750 3700 5650
Wire Wire Line
	3700 5850 3700 5750
Wire Wire Line
	3700 5950 4250 5950
Wire Wire Line
	3700 6050 4250 6050
Wire Wire Line
	3700 6150 4250 6150
Wire Wire Line
	3700 6250 4250 6250
Wire Wire Line
	3700 6350 4250 6350
Wire Wire Line
	3700 6450 4250 6450
Wire Wire Line
	3700 6550 4250 6550
Wire Wire Line
	3700 6650 4250 6650
Wire Wire Line
	3700 6750 4250 6750
Wire Wire Line
	3700 6850 4250 6850
Wire Wire Line
	3700 6950 4250 6950
Wire Wire Line
	3700 7050 4250 7050
Wire Wire Line
	3700 7150 4250 7150
Wire Wire Line
	3700 7250 4250 7250
Wire Wire Line
	3700 7350 4250 7350
Wire Wire Line
	3700 7450 4250 7450
Wire Wire Line
	3850 4650 3700 4650
Wire Wire Line
	3850 5550 3700 5550
Wire Wire Line
	4100 1050 3700 1050
Wire Bus Line
	4300 3950 4300 4150
Wire Bus Line
	4300 4150 4300 4300
Wire Bus Line
	4300 4300 4400 4300
Wire Bus Line
	4350 5750 4350 5950
Wire Bus Line
	4350 5750 4550 5750
Wire Bus Line
	4350 5950 4350 6150
Wire Bus Line
	4350 6150 4350 6350
Wire Bus Line
	4350 6350 4350 6450
Wire Bus Line
	4350 6450 4350 6650
Wire Bus Line
	4350 6650 4350 6850
Wire Bus Line
	4350 6850 4350 7050
Wire Bus Line
	4350 7050 4350 7250
Wire Bus Line
	4350 7250 4350 7350
Wire Bus Line
	4400 1950 4400 2050
Wire Bus Line
	4400 2050 4400 2250
Wire Bus Line
	4400 2250 4400 2450
Wire Bus Line
	4400 2450 4400 2650
Wire Bus Line
	4400 2650 4400 2850
Wire Bus Line
	4400 2850 4400 3050
Wire Bus Line
	4400 3050 4400 3250
Wire Bus Line
	4400 3250 4400 3450
Wire Bus Line
	4400 3450 4400 3650
Wire Bus Line
	4400 3650 4400 3750
Wire Bus Line
	4400 3750 4400 3950
Wire Bus Line
	4400 3950 4550 3950
Text Label 3750 1850 0    50   ~ 0
GFX-ADDR19
Text Label 3750 1950 0    50   ~ 0
GFX-ADDR18
Text Label 3750 2050 0    50   ~ 0
GFX-ADDR17
Text Label 3750 2150 0    50   ~ 0
GFX-ADDR16
Text Label 3750 2250 0    50   ~ 0
GFX-ADDR15
Text Label 3750 2350 0    50   ~ 0
GFX-ADDR14
Text Label 3750 2450 0    50   ~ 0
GFX-ADDR13
Text Label 3750 2550 0    50   ~ 0
GFX-ADDR12
Text Label 3750 2650 0    50   ~ 0
GFX-ADDR11
Text Label 3750 2750 0    50   ~ 0
GFX-ADDR10
Text Label 3750 2850 0    50   ~ 0
GFX-ADDR9
Text Label 3750 2950 0    50   ~ 0
GFX-ADDR8
Text Label 3750 3050 0    50   ~ 0
GFX-ADDR7
Text Label 3750 3150 0    50   ~ 0
GFX-ADDR6
Text Label 3750 3250 0    50   ~ 0
GFX-ADDR5
Text Label 3750 3350 0    50   ~ 0
GFX-ADDR4
Text Label 3750 3450 0    50   ~ 0
GFX-ADDR3
Text Label 3750 3550 0    50   ~ 0
GFX-ADDR2
Text Label 3750 3650 0    50   ~ 0
GFX-ADDR1
Text Label 3750 3750 0    50   ~ 0
GFX-ADDR0
Text Label 3750 3850 0    50   ~ 0
SECURITY5
Text Label 3750 3950 0    50   ~ 0
SECURITY4
Text Label 3750 4050 0    50   ~ 0
SECURITY3
Text Label 3750 4150 0    50   ~ 0
SECURITY2
Text Label 3750 5950 0    50   ~ 0
GFX-OUT0
Text Label 3750 6050 0    50   ~ 0
GFX-OUT2
Text Label 3750 6150 0    50   ~ 0
GFX-OUT4
Text Label 3750 6250 0    50   ~ 0
GFX-OUT6
Text Label 3750 6350 0    50   ~ 0
GFX-OUT8
Text Label 3750 6450 0    50   ~ 0
GFX-OUT9
Text Label 3750 6550 0    50   ~ 0
GFX-OUT10
Text Label 3750 6650 0    50   ~ 0
GFX-OUT11
Text Label 3750 6750 0    50   ~ 0
GFX-OUT12
Text Label 3750 6850 0    50   ~ 0
GFX-OUT13
Text Label 3750 6950 0    50   ~ 0
GFX-OUT14
Text Label 3750 7050 0    50   ~ 0
GFX-OUT15
Text Label 3750 7150 0    50   ~ 0
GFX-OUT7
Text Label 3750 7250 0    50   ~ 0
GFX-OUT5
Text Label 3750 7350 0    50   ~ 0
GFX-OUT3
Text Label 3750 7450 0    50   ~ 0
GFX-OUT1
Text HLabel 4100 1050 2    50   Output ~ 0
CLK16M
Text HLabel 4100 1150 2    50   Output ~ 0
CLK2M
Text HLabel 4100 1250 2    50   Output ~ 0
CLK4M
Text HLabel 4100 1350 2    50   Output ~ 0
CLK8M
Text HLabel 4100 1450 2    50   BiDi ~ 0
~VBLANK
Text HLabel 4100 1550 2    50   BiDi ~ 0
CIF158
Text HLabel 4250 1750 2    50   BiDi ~ 0
MIF54
Text HLabel 4400 4300 2    50   BiDi ~ 0
SECURITY[2..5]
Text HLabel 4550 3950 2    50   Input ~ 0
GFX-ADDR[0..19]
Text HLabel 4550 5750 2    50   Input ~ 0
GFX-OUT[0..15]
$Comp
L power:VCC #PWR?
U 1 1 640B4865
P 2500 3800
AR Path="/6492EFBF/640B4865" Ref="#PWR?"  Part="1" 
AR Path="/640B4865" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/640B4865" Ref="#PWR0256"  Part="1" 
F 0 "#PWR0256" H 2500 3650 50  0001 C CNN
F 1 "VCC" H 2517 3973 50  0000 C CNN
F 2 "" H 2500 3800 50  0001 C CNN
F 3 "" H 2500 3800 50  0001 C CNN
	1    2500 3800
	-1   0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 644699CF
P 3850 5550
AR Path="/6492EFBF/644699CF" Ref="#PWR?"  Part="1" 
AR Path="/644699CF" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/644699CF" Ref="#PWR0258"  Part="1" 
F 0 "#PWR0258" H 3850 5400 50  0001 C CNN
F 1 "VCC" H 3867 5723 50  0000 C CNN
F 2 "" H 3850 5550 50  0001 C CNN
F 3 "" H 3850 5550 50  0001 C CNN
	1    3850 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 640B486B
P 2500 5500
AR Path="/6492EFBF/640B486B" Ref="#PWR?"  Part="1" 
AR Path="/640B486B" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/640B486B" Ref="#PWR0257"  Part="1" 
F 0 "#PWR0257" H 2500 5250 50  0001 C CNN
F 1 "GND" H 2505 5327 50  0001 C CNN
F 2 "" H 2500 5500 50  0001 C CNN
F 3 "" H 2500 5500 50  0001 C CNN
	1    2500 5500
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6446DE90
P 3850 4650
AR Path="/6492EFBF/6446DE90" Ref="#PWR?"  Part="1" 
AR Path="/6446DE90" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/6446DE90" Ref="#PWR0259"  Part="1" 
F 0 "#PWR0259" H 3850 4400 50  0001 C CNN
F 1 "GND" H 3855 4477 50  0001 C CNN
F 2 "" H 3850 4650 50  0001 C CNN
F 3 "" H 3850 4650 50  0001 C CNN
	1    3850 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64471178
P 4600 1650
AR Path="/6492EFBF/64471178" Ref="#PWR?"  Part="1" 
AR Path="/64471178" Ref="#PWR?"  Part="1" 
AR Path="/640ADD2B/64471178" Ref="#PWR0260"  Part="1" 
F 0 "#PWR0260" H 4600 1400 50  0001 C CNN
F 1 "GND" H 4605 1477 50  0001 C CNN
F 2 "" H 4600 1650 50  0001 C CNN
F 3 "" H 4600 1650 50  0001 C CNN
	1    4600 1650
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:96PIN_Sep CN2
U 1 1 640AF005
P 3200 4250
F 0 "CN2" V -173 4225 50  0000 C CNN
F 1 "96PIN_Sep" V -174 4225 50  0001 C CNN
F 2 "" H 2000 4550 50  0001 C CNN
F 3 "" H 2000 4550 50  0001 C CNN
	1    3200 4250
	0    1    1    0   
$EndComp
$EndSCHEMATC
