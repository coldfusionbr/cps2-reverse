#  93646B Reverse Engineering

![CPS2 B Board](pcb-images/93646B.jpg)

## PDF Render

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/93646B.pdf).

[![CPS2 B Board](https://petitl.fr/cps2/93646B.svg)](https://petitl.fr/cps2/93646B.pdf)

## Notes
### Custom DL
Most of the reverse engineering has been done without the knowledge of the insides of the custom DLs.

Future work will try to reverse the DL decap. As Eduardo Cruz demonstrated, a lot of the custom DL don't do processing and are basically passthroughts, those should be accessible without a lot of complication and will greatly help in the understanding of the CIF.

### Revisions
There are 5 known revisions of this board (not counting the black all-in-one board) from `-3` to `-7`.
* `4`: `CN8` changes to a more accessible place
* `5`: `CN9` security injection is inserted but not powered. Routing error on `QSound` for the `PAL BGSB3` is now fixed. Additional optional pull-up resistors are also added. Useless ICs 10J and 10K are removed from the board. An additional slot with a different size has been added for the optional EEPROM.
* `6`: `CN9` is correctly powered now.
* `7`: ??

The reverse engineered schema is based on the `6+` with the details of `10J` and `10K`. Some pull-ups (`R13+`) from the `5+` revisions may be missing.

### Bus discovery and ordering
No intensive research has been done on the buses. Basically, if a group of 16 wires with pull-ups went from A to B, then there is a (data) bus but the order may not be relevant. I tried to have it consistent, most of the design involve the following relationship: ascending pin numbers = descending bus order, obviously it's far from the universal truth but that's a good base ground.

Similarly, the exact pin on each resistor array may not correspond to the exact wire but it will definitely be the same bus.

## PCB Images
Those images were very useful in reversing everything/

| Image | Description | Source and credits if known |
| ------ | ------ | ------ |
| [93646B.jpg](pcb-images/93646B.jpg) | Low-res view for display | WydD |
| [all-dl-below.jpg](pcb-images/all-dl-below.jpg) | Complete PCB without DLs | I'm not sure but I think it was on a repair log on arcadeotaku maybe by leonardoliveira |
| [avalaunch-bottom.jpg](pcb-images/avalaunch-bottom.jpg) | Bottom without CN1 and CN2 | Was on the (now dead) forum of avalaunch on the security investigation thread |
| [avalaunch-top.jpg](pcb-images/avalaunch-top.jpg) | Top without most of the left side component | Same as the bottom side |
| [cn1-ls245-below.jpg](pcb-images/cn1-ls245-below.jpg) | Below the CN1 bus transceivers | WydD |
| [cn2-ls245-below.jpg](pcb-images/cn2-ls245-below.jpg) | Below the CN2 bus transceivers | WydD |
| [dl-1927-below.png](pcb-images/dl-1927-below.png) | Close-up shot of the DL-1927 traces | Found on a [JammaArcade repair log](https://www.jammarcade.net/cps2-xmen-children-of-the-atom-repair/) |
| [dl-2027-below.png](pcb-images/dl-2027-below.png) | Close-up shot of the DL-2027 traces | Same as `all-dl-below` |
| [wydd-top.png](pcb-images/wydd-top.png) | High-res general view of a rev 6 | WydD |
| [wydd-bottom.png](pcb-images/dl-2027-below.png) | High-res bottom view of a rev 4 | WydD |

## Related links
* [B-Board Wiring Spreadsheet](https://docs.google.com/spreadsheets/d/1SGXb6cE9Uu68Lmt0O0knL34eWvkaVA7-mJ8i6F27lmk/edit#gid=0) by WydD. That's what triggered this project.
* PALs have been dumped by contributors of [jammaarcade.net](https://www.jammarcade.net/pal-dumps/). You can use jedutil (found in MAME) to inspect their equation.
* [Eduardo Cruz, A journey into Capcom's CPS2 Silicon](http://arcadehacker.blogspot.com/2017/03/a-journey-into-capcoms-cps2-silicon.html).

## Authors & Licence
See main README.
