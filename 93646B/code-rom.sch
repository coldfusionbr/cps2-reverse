EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 17
Title "Code ROM"
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 4450 3650
Connection ~ 2750 3650
Connection ~ 4450 7150
Connection ~ 2400 4150
Connection ~ 2750 7150
Connection ~ 5600 3950
Connection ~ 3900 3950
Connection ~ 9200 4500
Connection ~ 9200 5800
Connection ~ 7300 3950
Connection ~ 6650 4450
Connection ~ 4950 4450
Connection ~ 1550 4450
Connection ~ 6650 950 
Connection ~ 4950 950 
Connection ~ 3250 950 
Connection ~ 1550 950 
Connection ~ 4900 6850
Connection ~ 1500 6850
Connection ~ 6600 6850
Connection ~ 6600 3350
Connection ~ 4900 3350
Connection ~ 3200 3350
Connection ~ 1500 3350
Connection ~ 6450 4450
Connection ~ 4750 4450
Connection ~ 1350 4450
Connection ~ 6450 950 
Connection ~ 4750 950 
Connection ~ 3050 950 
Connection ~ 1350 950 
Connection ~ 3050 4450
Connection ~ 3200 6850
Connection ~ 3250 4450
Connection ~ 1050 7150
Connection ~ 4100 4150
Connection ~ 2200 3950
Connection ~ 700  4150
Connection ~ 5800 4150
Connection ~ 1050 3650
Connection ~ 6150 7150
Connection ~ 8300 4150
Connection ~ 9250 2650
Connection ~ 10150 2650
Connection ~ 9200 700 
Connection ~ 8300 700 
NoConn ~ 8700 3850
Entry Wire Line
	700  1250 800  1150
Entry Wire Line
	700  1350 800  1250
Entry Wire Line
	700  1450 800  1350
Entry Wire Line
	700  1550 800  1450
Entry Wire Line
	700  1650 800  1550
Entry Wire Line
	700  1750 800  1650
Entry Wire Line
	700  1850 800  1750
Entry Wire Line
	700  1950 800  1850
Entry Wire Line
	700  2050 800  1950
Entry Wire Line
	700  2150 800  2050
Entry Wire Line
	700  2250 800  2150
Entry Wire Line
	700  2350 800  2250
Entry Wire Line
	700  2450 800  2350
Entry Wire Line
	700  2550 800  2450
Entry Wire Line
	700  2650 800  2550
Entry Wire Line
	700  2750 800  2650
Entry Wire Line
	700  2850 800  2750
Entry Wire Line
	700  2950 800  2850
Entry Wire Line
	700  4550 800  4650
Entry Wire Line
	700  4650 800  4750
Entry Wire Line
	700  4750 800  4850
Entry Wire Line
	700  4850 800  4950
Entry Wire Line
	700  4950 800  5050
Entry Wire Line
	700  5050 800  5150
Entry Wire Line
	700  5150 800  5250
Entry Wire Line
	700  5250 800  5350
Entry Wire Line
	700  5350 800  5450
Entry Wire Line
	700  5450 800  5550
Entry Wire Line
	700  5550 800  5650
Entry Wire Line
	700  5650 800  5750
Entry Wire Line
	700  5750 800  5850
Entry Wire Line
	700  5850 800  5950
Entry Wire Line
	700  5950 800  6050
Entry Wire Line
	700  6050 800  6150
Entry Wire Line
	700  6150 800  6250
Entry Wire Line
	700  6250 800  6350
Entry Wire Line
	2100 1150 2200 1250
Entry Wire Line
	2100 1250 2200 1350
Entry Wire Line
	2100 1350 2200 1450
Entry Wire Line
	2100 1450 2200 1550
Entry Wire Line
	2100 1550 2200 1650
Entry Wire Line
	2100 1650 2200 1750
Entry Wire Line
	2100 1750 2200 1850
Entry Wire Line
	2100 1850 2200 1950
Entry Wire Line
	2100 1950 2200 2050
Entry Wire Line
	2100 2050 2200 2150
Entry Wire Line
	2100 2150 2200 2250
Entry Wire Line
	2100 2250 2200 2350
Entry Wire Line
	2100 2350 2200 2450
Entry Wire Line
	2100 2450 2200 2550
Entry Wire Line
	2100 2550 2200 2650
Entry Wire Line
	2100 2650 2200 2750
Entry Wire Line
	2100 4650 2200 4550
Entry Wire Line
	2100 4750 2200 4650
Entry Wire Line
	2100 4850 2200 4750
Entry Wire Line
	2100 4950 2200 4850
Entry Wire Line
	2100 5050 2200 4950
Entry Wire Line
	2100 5150 2200 5050
Entry Wire Line
	2100 5250 2200 5150
Entry Wire Line
	2100 5350 2200 5250
Entry Wire Line
	2100 5450 2200 5350
Entry Wire Line
	2100 5550 2200 5450
Entry Wire Line
	2100 5650 2200 5550
Entry Wire Line
	2100 5750 2200 5650
Entry Wire Line
	2100 5850 2200 5750
Entry Wire Line
	2100 5950 2200 5850
Entry Wire Line
	2100 6050 2200 5950
Entry Wire Line
	2100 6150 2200 6050
Entry Wire Line
	2400 1250 2500 1150
Entry Wire Line
	2400 1350 2500 1250
Entry Wire Line
	2400 1450 2500 1350
Entry Wire Line
	2400 1550 2500 1450
Entry Wire Line
	2400 1650 2500 1550
Entry Wire Line
	2400 1750 2500 1650
Entry Wire Line
	2400 1850 2500 1750
Entry Wire Line
	2400 1950 2500 1850
Entry Wire Line
	2400 2050 2500 1950
Entry Wire Line
	2400 2150 2500 2050
Entry Wire Line
	2400 2250 2500 2150
Entry Wire Line
	2400 2350 2500 2250
Entry Wire Line
	2400 2450 2500 2350
Entry Wire Line
	2400 2550 2500 2450
Entry Wire Line
	2400 2650 2500 2550
Entry Wire Line
	2400 2750 2500 2650
Entry Wire Line
	2400 2850 2500 2750
Entry Wire Line
	2400 2950 2500 2850
Entry Wire Line
	2400 4550 2500 4650
Entry Wire Line
	2400 4650 2500 4750
Entry Wire Line
	2400 4750 2500 4850
Entry Wire Line
	2400 4850 2500 4950
Entry Wire Line
	2400 4950 2500 5050
Entry Wire Line
	2400 5050 2500 5150
Entry Wire Line
	2400 5150 2500 5250
Entry Wire Line
	2400 5250 2500 5350
Entry Wire Line
	2400 5350 2500 5450
Entry Wire Line
	2400 5450 2500 5550
Entry Wire Line
	2400 5550 2500 5650
Entry Wire Line
	2400 5650 2500 5750
Entry Wire Line
	2400 5750 2500 5850
Entry Wire Line
	2400 5850 2500 5950
Entry Wire Line
	2400 5950 2500 6050
Entry Wire Line
	2400 6050 2500 6150
Entry Wire Line
	2400 6150 2500 6250
Entry Wire Line
	2400 6250 2500 6350
Entry Wire Line
	3800 1150 3900 1250
Entry Wire Line
	3800 1250 3900 1350
Entry Wire Line
	3800 1350 3900 1450
Entry Wire Line
	3800 1450 3900 1550
Entry Wire Line
	3800 1550 3900 1650
Entry Wire Line
	3800 1650 3900 1750
Entry Wire Line
	3800 1750 3900 1850
Entry Wire Line
	3800 1850 3900 1950
Entry Wire Line
	3800 1950 3900 2050
Entry Wire Line
	3800 2050 3900 2150
Entry Wire Line
	3800 2150 3900 2250
Entry Wire Line
	3800 2250 3900 2350
Entry Wire Line
	3800 2350 3900 2450
Entry Wire Line
	3800 2450 3900 2550
Entry Wire Line
	3800 2550 3900 2650
Entry Wire Line
	3800 2650 3900 2750
Entry Wire Line
	3800 4650 3900 4550
Entry Wire Line
	3800 4750 3900 4650
Entry Wire Line
	3800 4850 3900 4750
Entry Wire Line
	3800 4950 3900 4850
Entry Wire Line
	3800 5050 3900 4950
Entry Wire Line
	3800 5150 3900 5050
Entry Wire Line
	3800 5250 3900 5150
Entry Wire Line
	3800 5350 3900 5250
Entry Wire Line
	3800 5450 3900 5350
Entry Wire Line
	3800 5550 3900 5450
Entry Wire Line
	3800 5650 3900 5550
Entry Wire Line
	3800 5750 3900 5650
Entry Wire Line
	3800 5850 3900 5750
Entry Wire Line
	3800 5950 3900 5850
Entry Wire Line
	3800 6050 3900 5950
Entry Wire Line
	3800 6150 3900 6050
Entry Wire Line
	4100 1250 4200 1150
Entry Wire Line
	4100 1350 4200 1250
Entry Wire Line
	4100 1450 4200 1350
Entry Wire Line
	4100 1550 4200 1450
Entry Wire Line
	4100 1650 4200 1550
Entry Wire Line
	4100 1750 4200 1650
Entry Wire Line
	4100 1850 4200 1750
Entry Wire Line
	4100 1950 4200 1850
Entry Wire Line
	4100 2050 4200 1950
Entry Wire Line
	4100 2150 4200 2050
Entry Wire Line
	4100 2250 4200 2150
Entry Wire Line
	4100 2350 4200 2250
Entry Wire Line
	4100 2450 4200 2350
Entry Wire Line
	4100 2550 4200 2450
Entry Wire Line
	4100 2650 4200 2550
Entry Wire Line
	4100 2750 4200 2650
Entry Wire Line
	4100 2850 4200 2750
Entry Wire Line
	4100 2950 4200 2850
Entry Wire Line
	4100 4550 4200 4650
Entry Wire Line
	4100 4650 4200 4750
Entry Wire Line
	4100 4750 4200 4850
Entry Wire Line
	4100 4850 4200 4950
Entry Wire Line
	4100 4950 4200 5050
Entry Wire Line
	4100 5050 4200 5150
Entry Wire Line
	4100 5150 4200 5250
Entry Wire Line
	4100 5250 4200 5350
Entry Wire Line
	4100 5350 4200 5450
Entry Wire Line
	4100 5450 4200 5550
Entry Wire Line
	4100 5550 4200 5650
Entry Wire Line
	4100 5650 4200 5750
Entry Wire Line
	4100 5750 4200 5850
Entry Wire Line
	4100 5850 4200 5950
Entry Wire Line
	4100 5950 4200 6050
Entry Wire Line
	4100 6050 4200 6150
Entry Wire Line
	4100 6150 4200 6250
Entry Wire Line
	4100 6250 4200 6350
Entry Wire Line
	5500 1150 5600 1250
Entry Wire Line
	5500 1250 5600 1350
Entry Wire Line
	5500 1350 5600 1450
Entry Wire Line
	5500 1450 5600 1550
Entry Wire Line
	5500 1550 5600 1650
Entry Wire Line
	5500 1650 5600 1750
Entry Wire Line
	5500 1750 5600 1850
Entry Wire Line
	5500 1850 5600 1950
Entry Wire Line
	5500 1950 5600 2050
Entry Wire Line
	5500 2050 5600 2150
Entry Wire Line
	5500 2150 5600 2250
Entry Wire Line
	5500 2250 5600 2350
Entry Wire Line
	5500 2350 5600 2450
Entry Wire Line
	5500 2450 5600 2550
Entry Wire Line
	5500 2550 5600 2650
Entry Wire Line
	5500 2650 5600 2750
Entry Wire Line
	5500 4650 5600 4550
Entry Wire Line
	5500 4750 5600 4650
Entry Wire Line
	5500 4850 5600 4750
Entry Wire Line
	5500 4950 5600 4850
Entry Wire Line
	5500 5050 5600 4950
Entry Wire Line
	5500 5150 5600 5050
Entry Wire Line
	5500 5250 5600 5150
Entry Wire Line
	5500 5350 5600 5250
Entry Wire Line
	5500 5450 5600 5350
Entry Wire Line
	5500 5550 5600 5450
Entry Wire Line
	5500 5650 5600 5550
Entry Wire Line
	5500 5750 5600 5650
Entry Wire Line
	5500 5850 5600 5750
Entry Wire Line
	5500 5950 5600 5850
Entry Wire Line
	5500 6050 5600 5950
Entry Wire Line
	5500 6150 5600 6050
Entry Wire Line
	5800 1250 5900 1150
Entry Wire Line
	5800 1350 5900 1250
Entry Wire Line
	5800 1450 5900 1350
Entry Wire Line
	5800 1550 5900 1450
Entry Wire Line
	5800 1650 5900 1550
Entry Wire Line
	5800 1750 5900 1650
Entry Wire Line
	5800 1850 5900 1750
Entry Wire Line
	5800 1950 5900 1850
Entry Wire Line
	5800 2050 5900 1950
Entry Wire Line
	5800 2150 5900 2050
Entry Wire Line
	5800 2250 5900 2150
Entry Wire Line
	5800 2350 5900 2250
Entry Wire Line
	5800 2450 5900 2350
Entry Wire Line
	5800 2550 5900 2450
Entry Wire Line
	5800 2650 5900 2550
Entry Wire Line
	5800 2750 5900 2650
Entry Wire Line
	5800 2850 5900 2750
Entry Wire Line
	5800 2950 5900 2850
Entry Wire Line
	5800 4550 5900 4650
Entry Wire Line
	5800 4650 5900 4750
Entry Wire Line
	5800 4750 5900 4850
Entry Wire Line
	5800 4850 5900 4950
Entry Wire Line
	5800 4950 5900 5050
Entry Wire Line
	5800 5050 5900 5150
Entry Wire Line
	5800 5150 5900 5250
Entry Wire Line
	5800 5250 5900 5350
Entry Wire Line
	5800 5350 5900 5450
Entry Wire Line
	5800 5450 5900 5550
Entry Wire Line
	5800 5550 5900 5650
Entry Wire Line
	5800 5650 5900 5750
Entry Wire Line
	5800 5750 5900 5850
Entry Wire Line
	5800 5850 5900 5950
Entry Wire Line
	5800 5950 5900 6050
Entry Wire Line
	5800 6050 5900 6150
Entry Wire Line
	5800 6150 5900 6250
Entry Wire Line
	5800 6250 5900 6350
Entry Wire Line
	7200 1150 7300 1250
Entry Wire Line
	7200 1250 7300 1350
Entry Wire Line
	7200 1350 7300 1450
Entry Wire Line
	7200 1450 7300 1550
Entry Wire Line
	7200 1550 7300 1650
Entry Wire Line
	7200 1650 7300 1750
Entry Wire Line
	7200 1750 7300 1850
Entry Wire Line
	7200 1850 7300 1950
Entry Wire Line
	7200 1950 7300 2050
Entry Wire Line
	7200 2050 7300 2150
Entry Wire Line
	7200 2150 7300 2250
Entry Wire Line
	7200 2250 7300 2350
Entry Wire Line
	7200 2350 7300 2450
Entry Wire Line
	7200 2450 7300 2550
Entry Wire Line
	7200 2550 7300 2650
Entry Wire Line
	7200 2650 7300 2750
Entry Wire Line
	7300 4550 7200 4650
Entry Wire Line
	7300 4650 7200 4750
Entry Wire Line
	7300 4750 7200 4850
Entry Wire Line
	7300 4850 7200 4950
Entry Wire Line
	7300 4950 7200 5050
Entry Wire Line
	7300 5050 7200 5150
Entry Wire Line
	7300 5150 7200 5250
Entry Wire Line
	7300 5250 7200 5350
Entry Wire Line
	7300 5350 7200 5450
Entry Wire Line
	7300 5450 7200 5550
Entry Wire Line
	7300 5550 7200 5650
Entry Wire Line
	7300 5650 7200 5750
Entry Wire Line
	7300 5750 7200 5850
Entry Wire Line
	7300 5850 7200 5950
Entry Wire Line
	7300 5950 7200 6050
Entry Wire Line
	7300 6050 7200 6150
Entry Wire Line
	8300 1100 8400 1200
Entry Wire Line
	8300 1200 8400 1300
Entry Wire Line
	8300 1300 8400 1400
Entry Wire Line
	8300 1400 8400 1500
Entry Wire Line
	8300 1500 8400 1600
Entry Wire Line
	8300 1600 8400 1700
Entry Wire Line
	8300 1700 8400 1800
Entry Wire Line
	8300 1800 8400 1900
Entry Wire Line
	8300 3050 8400 3150
Entry Wire Line
	8300 3150 8400 3250
Entry Wire Line
	8300 3250 8400 3350
Entry Wire Line
	8300 3350 8400 3450
Entry Wire Line
	8300 3450 8400 3550
Entry Wire Line
	8300 3550 8400 3650
Entry Wire Line
	8300 3650 8400 3750
Entry Wire Line
	8300 4600 8400 4700
Entry Wire Line
	8300 4700 8400 4800
Entry Wire Line
	8300 4800 8400 4900
Entry Wire Line
	8300 4900 8400 5000
Entry Wire Line
	8300 5000 8400 5100
Entry Wire Line
	8300 5100 8400 5200
Entry Wire Line
	8300 5200 8400 5300
Entry Wire Line
	8300 5300 8400 5400
Entry Wire Line
	9200 1100 9300 1200
Entry Wire Line
	9200 1200 9300 1300
Entry Wire Line
	9200 1300 9300 1400
Entry Wire Line
	9200 1400 9300 1500
Entry Wire Line
	9200 1500 9300 1600
Entry Wire Line
	9200 1600 9300 1700
Entry Wire Line
	9200 1700 9300 1800
Entry Wire Line
	9200 1800 9300 1900
Entry Wire Line
	9250 3050 9350 3150
Entry Wire Line
	9250 3150 9350 3250
Entry Wire Line
	9250 3250 9350 3350
Entry Wire Line
	9250 3350 9350 3450
Entry Wire Line
	9250 3450 9350 3550
Entry Wire Line
	9250 3550 9350 3650
Entry Wire Line
	9250 3650 9350 3750
Entry Wire Line
	9250 3750 9350 3850
Entry Wire Line
	10150 3050 10250 3150
Entry Wire Line
	10150 3150 10250 3250
Entry Wire Line
	10150 3250 10250 3350
Entry Wire Line
	10150 3350 10250 3450
Entry Wire Line
	10150 3450 10250 3550
Entry Wire Line
	10150 3550 10250 3650
Entry Wire Line
	10150 3650 10250 3750
Entry Wire Line
	10150 3750 10250 3850
Wire Wire Line
	550  3650 550  7150
Wire Wire Line
	550  3650 1050 3650
Wire Wire Line
	550  7150 1050 7150
Wire Wire Line
	800  1150 1050 1150
Wire Wire Line
	800  1250 1050 1250
Wire Wire Line
	800  1350 1050 1350
Wire Wire Line
	800  1450 1050 1450
Wire Wire Line
	800  1550 1050 1550
Wire Wire Line
	800  1650 1050 1650
Wire Wire Line
	800  1750 1050 1750
Wire Wire Line
	800  1850 1050 1850
Wire Wire Line
	800  1950 1050 1950
Wire Wire Line
	800  2050 1050 2050
Wire Wire Line
	800  2150 1050 2150
Wire Wire Line
	800  2250 1050 2250
Wire Wire Line
	800  2350 1050 2350
Wire Wire Line
	800  2450 1050 2450
Wire Wire Line
	800  2550 1050 2550
Wire Wire Line
	800  2650 1050 2650
Wire Wire Line
	800  2750 1050 2750
Wire Wire Line
	800  2850 1050 2850
Wire Wire Line
	800  4650 1050 4650
Wire Wire Line
	800  4750 1050 4750
Wire Wire Line
	800  4850 1050 4850
Wire Wire Line
	800  4950 1050 4950
Wire Wire Line
	800  5050 1050 5050
Wire Wire Line
	800  5150 1050 5150
Wire Wire Line
	800  5250 1050 5250
Wire Wire Line
	800  5350 1050 5350
Wire Wire Line
	800  5450 1050 5450
Wire Wire Line
	800  5550 1050 5550
Wire Wire Line
	800  5650 1050 5650
Wire Wire Line
	800  5750 1050 5750
Wire Wire Line
	800  5850 1050 5850
Wire Wire Line
	800  5950 1050 5950
Wire Wire Line
	800  6050 1050 6050
Wire Wire Line
	800  6150 1050 6150
Wire Wire Line
	800  6250 1050 6250
Wire Wire Line
	800  6350 1050 6350
Wire Wire Line
	850  3050 1050 3050
Wire Wire Line
	850  6550 1050 6550
Wire Wire Line
	1050 3150 1050 3650
Wire Wire Line
	1050 3650 2750 3650
Wire Wire Line
	1050 6650 1050 7150
Wire Wire Line
	1050 7150 2750 7150
Wire Wire Line
	1150 950  1350 950 
Wire Wire Line
	1150 4450 1350 4450
Wire Wire Line
	1350 950  1550 950 
Wire Wire Line
	1350 4450 1550 4450
Wire Wire Line
	1400 3350 1500 3350
Wire Wire Line
	1400 6850 1500 6850
Wire Wire Line
	1700 950  1550 950 
Wire Wire Line
	1700 4450 1550 4450
Wire Wire Line
	1850 1150 2100 1150
Wire Wire Line
	1850 1250 2100 1250
Wire Wire Line
	1850 1350 2100 1350
Wire Wire Line
	1850 1450 2100 1450
Wire Wire Line
	1850 1550 2100 1550
Wire Wire Line
	1850 1650 2100 1650
Wire Wire Line
	1850 1750 2100 1750
Wire Wire Line
	1850 1850 2100 1850
Wire Wire Line
	1850 1950 2100 1950
Wire Wire Line
	1850 2050 2100 2050
Wire Wire Line
	1850 2150 2100 2150
Wire Wire Line
	1850 2250 2100 2250
Wire Wire Line
	1850 2350 2100 2350
Wire Wire Line
	1850 2450 2100 2450
Wire Wire Line
	1850 2550 2100 2550
Wire Wire Line
	1850 2650 2100 2650
Wire Wire Line
	1850 4650 2100 4650
Wire Wire Line
	1850 4750 2100 4750
Wire Wire Line
	1850 4850 2100 4850
Wire Wire Line
	1850 4950 2100 4950
Wire Wire Line
	1850 5050 2100 5050
Wire Wire Line
	1850 5150 2100 5150
Wire Wire Line
	1850 5250 2100 5250
Wire Wire Line
	1850 5350 2100 5350
Wire Wire Line
	1850 5450 2100 5450
Wire Wire Line
	1850 5550 2100 5550
Wire Wire Line
	1850 5650 2100 5650
Wire Wire Line
	1850 5750 2100 5750
Wire Wire Line
	1850 5850 2100 5850
Wire Wire Line
	1850 5950 2100 5950
Wire Wire Line
	1850 6050 2100 6050
Wire Wire Line
	1850 6150 2100 6150
Wire Wire Line
	2500 1150 2750 1150
Wire Wire Line
	2500 1250 2750 1250
Wire Wire Line
	2500 1350 2750 1350
Wire Wire Line
	2500 1450 2750 1450
Wire Wire Line
	2500 1550 2750 1550
Wire Wire Line
	2500 1650 2750 1650
Wire Wire Line
	2500 1750 2750 1750
Wire Wire Line
	2500 1850 2750 1850
Wire Wire Line
	2500 1950 2750 1950
Wire Wire Line
	2500 2050 2750 2050
Wire Wire Line
	2500 2150 2750 2150
Wire Wire Line
	2500 2250 2750 2250
Wire Wire Line
	2500 2350 2750 2350
Wire Wire Line
	2500 2450 2750 2450
Wire Wire Line
	2500 2550 2750 2550
Wire Wire Line
	2500 2650 2750 2650
Wire Wire Line
	2500 2750 2750 2750
Wire Wire Line
	2500 2850 2750 2850
Wire Wire Line
	2500 3050 2750 3050
Wire Wire Line
	2500 4650 2750 4650
Wire Wire Line
	2500 4750 2750 4750
Wire Wire Line
	2500 4850 2750 4850
Wire Wire Line
	2500 4950 2750 4950
Wire Wire Line
	2500 5050 2750 5050
Wire Wire Line
	2500 5150 2750 5150
Wire Wire Line
	2500 5250 2750 5250
Wire Wire Line
	2500 5350 2750 5350
Wire Wire Line
	2500 5450 2750 5450
Wire Wire Line
	2500 5550 2750 5550
Wire Wire Line
	2500 5650 2750 5650
Wire Wire Line
	2500 5750 2750 5750
Wire Wire Line
	2500 5850 2750 5850
Wire Wire Line
	2500 5950 2750 5950
Wire Wire Line
	2500 6050 2750 6050
Wire Wire Line
	2500 6150 2750 6150
Wire Wire Line
	2500 6250 2750 6250
Wire Wire Line
	2500 6350 2750 6350
Wire Wire Line
	2500 6550 2750 6550
Wire Wire Line
	2750 3650 2750 3150
Wire Wire Line
	2750 3650 4450 3650
Wire Wire Line
	2750 7150 2750 6650
Wire Wire Line
	2750 7150 4450 7150
Wire Wire Line
	2850 950  3050 950 
Wire Wire Line
	2850 4450 3050 4450
Wire Wire Line
	3050 950  3250 950 
Wire Wire Line
	3050 4450 3250 4450
Wire Wire Line
	3100 3350 3200 3350
Wire Wire Line
	3100 6850 3200 6850
Wire Wire Line
	3400 950  3250 950 
Wire Wire Line
	3400 4450 3250 4450
Wire Wire Line
	3550 1150 3800 1150
Wire Wire Line
	3550 1250 3800 1250
Wire Wire Line
	3550 1350 3800 1350
Wire Wire Line
	3550 1450 3800 1450
Wire Wire Line
	3550 1550 3800 1550
Wire Wire Line
	3550 1650 3800 1650
Wire Wire Line
	3550 1750 3800 1750
Wire Wire Line
	3550 1850 3800 1850
Wire Wire Line
	3550 1950 3800 1950
Wire Wire Line
	3550 2050 3800 2050
Wire Wire Line
	3550 2150 3800 2150
Wire Wire Line
	3550 2250 3800 2250
Wire Wire Line
	3550 2350 3800 2350
Wire Wire Line
	3550 2450 3800 2450
Wire Wire Line
	3550 2550 3800 2550
Wire Wire Line
	3550 2650 3800 2650
Wire Wire Line
	3550 4650 3800 4650
Wire Wire Line
	3550 4750 3800 4750
Wire Wire Line
	3550 4850 3800 4850
Wire Wire Line
	3550 4950 3800 4950
Wire Wire Line
	3550 5050 3800 5050
Wire Wire Line
	3550 5150 3800 5150
Wire Wire Line
	3550 5250 3800 5250
Wire Wire Line
	3550 5350 3800 5350
Wire Wire Line
	3550 5450 3800 5450
Wire Wire Line
	3550 5550 3800 5550
Wire Wire Line
	3550 5650 3800 5650
Wire Wire Line
	3550 5750 3800 5750
Wire Wire Line
	3550 5850 3800 5850
Wire Wire Line
	3550 5950 3800 5950
Wire Wire Line
	3550 6050 3800 6050
Wire Wire Line
	3550 6150 3800 6150
Wire Wire Line
	4200 1150 4450 1150
Wire Wire Line
	4200 1250 4450 1250
Wire Wire Line
	4200 1350 4450 1350
Wire Wire Line
	4200 1450 4450 1450
Wire Wire Line
	4200 1550 4450 1550
Wire Wire Line
	4200 1650 4450 1650
Wire Wire Line
	4200 1750 4450 1750
Wire Wire Line
	4200 1850 4450 1850
Wire Wire Line
	4200 1950 4450 1950
Wire Wire Line
	4200 2050 4450 2050
Wire Wire Line
	4200 2150 4450 2150
Wire Wire Line
	4200 2250 4450 2250
Wire Wire Line
	4200 2350 4450 2350
Wire Wire Line
	4200 2450 4450 2450
Wire Wire Line
	4200 2550 4450 2550
Wire Wire Line
	4200 2650 4450 2650
Wire Wire Line
	4200 2750 4450 2750
Wire Wire Line
	4200 2850 4450 2850
Wire Wire Line
	4200 3050 4450 3050
Wire Wire Line
	4200 4650 4450 4650
Wire Wire Line
	4200 4750 4450 4750
Wire Wire Line
	4200 4850 4450 4850
Wire Wire Line
	4200 4950 4450 4950
Wire Wire Line
	4200 5050 4450 5050
Wire Wire Line
	4200 5150 4450 5150
Wire Wire Line
	4200 5250 4450 5250
Wire Wire Line
	4200 5350 4450 5350
Wire Wire Line
	4200 5450 4450 5450
Wire Wire Line
	4200 5550 4450 5550
Wire Wire Line
	4200 5650 4450 5650
Wire Wire Line
	4200 5750 4450 5750
Wire Wire Line
	4200 5850 4450 5850
Wire Wire Line
	4200 5950 4450 5950
Wire Wire Line
	4200 6050 4450 6050
Wire Wire Line
	4200 6150 4450 6150
Wire Wire Line
	4200 6250 4450 6250
Wire Wire Line
	4200 6350 4450 6350
Wire Wire Line
	4200 6550 4450 6550
Wire Wire Line
	4450 3150 4450 3650
Wire Wire Line
	4450 3650 6150 3650
Wire Wire Line
	4450 6650 4450 7150
Wire Wire Line
	4450 7150 6150 7150
Wire Wire Line
	4550 950  4750 950 
Wire Wire Line
	4550 4450 4750 4450
Wire Wire Line
	4750 950  4950 950 
Wire Wire Line
	4750 4450 4950 4450
Wire Wire Line
	4800 3350 4900 3350
Wire Wire Line
	4800 6850 4900 6850
Wire Wire Line
	5100 950  4950 950 
Wire Wire Line
	5100 4450 4950 4450
Wire Wire Line
	5250 1150 5500 1150
Wire Wire Line
	5250 1250 5500 1250
Wire Wire Line
	5250 1350 5500 1350
Wire Wire Line
	5250 1450 5500 1450
Wire Wire Line
	5250 1550 5500 1550
Wire Wire Line
	5250 1650 5500 1650
Wire Wire Line
	5250 1750 5500 1750
Wire Wire Line
	5250 1850 5500 1850
Wire Wire Line
	5250 1950 5500 1950
Wire Wire Line
	5250 2050 5500 2050
Wire Wire Line
	5250 2150 5500 2150
Wire Wire Line
	5250 2250 5500 2250
Wire Wire Line
	5250 2350 5500 2350
Wire Wire Line
	5250 2450 5500 2450
Wire Wire Line
	5250 2550 5500 2550
Wire Wire Line
	5250 2650 5500 2650
Wire Wire Line
	5250 4650 5500 4650
Wire Wire Line
	5250 4750 5500 4750
Wire Wire Line
	5250 4850 5500 4850
Wire Wire Line
	5250 4950 5500 4950
Wire Wire Line
	5250 5050 5500 5050
Wire Wire Line
	5250 5150 5500 5150
Wire Wire Line
	5250 5250 5500 5250
Wire Wire Line
	5250 5350 5500 5350
Wire Wire Line
	5250 5450 5500 5450
Wire Wire Line
	5250 5550 5500 5550
Wire Wire Line
	5250 5650 5500 5650
Wire Wire Line
	5250 5750 5500 5750
Wire Wire Line
	5250 5850 5500 5850
Wire Wire Line
	5250 5950 5500 5950
Wire Wire Line
	5250 6050 5500 6050
Wire Wire Line
	5250 6150 5500 6150
Wire Wire Line
	5900 1150 6150 1150
Wire Wire Line
	5900 1250 6150 1250
Wire Wire Line
	5900 1350 6150 1350
Wire Wire Line
	5900 1450 6150 1450
Wire Wire Line
	5900 1550 6150 1550
Wire Wire Line
	5900 1650 6150 1650
Wire Wire Line
	5900 1750 6150 1750
Wire Wire Line
	5900 1850 6150 1850
Wire Wire Line
	5900 1950 6150 1950
Wire Wire Line
	5900 2050 6150 2050
Wire Wire Line
	5900 2150 6150 2150
Wire Wire Line
	5900 2250 6150 2250
Wire Wire Line
	5900 2350 6150 2350
Wire Wire Line
	5900 2450 6150 2450
Wire Wire Line
	5900 2550 6150 2550
Wire Wire Line
	5900 2650 6150 2650
Wire Wire Line
	5900 2750 6150 2750
Wire Wire Line
	5900 2850 6150 2850
Wire Wire Line
	5900 3050 6150 3050
Wire Wire Line
	5900 4650 6150 4650
Wire Wire Line
	5900 4750 6150 4750
Wire Wire Line
	5900 4850 6150 4850
Wire Wire Line
	5900 4950 6150 4950
Wire Wire Line
	5900 5050 6150 5050
Wire Wire Line
	5900 5150 6150 5150
Wire Wire Line
	5900 5250 6150 5250
Wire Wire Line
	5900 5350 6150 5350
Wire Wire Line
	5900 5450 6150 5450
Wire Wire Line
	5900 5550 6150 5550
Wire Wire Line
	5900 5650 6150 5650
Wire Wire Line
	5900 5750 6150 5750
Wire Wire Line
	5900 5850 6150 5850
Wire Wire Line
	5900 5950 6150 5950
Wire Wire Line
	5900 6050 6150 6050
Wire Wire Line
	5900 6150 6150 6150
Wire Wire Line
	5900 6250 6150 6250
Wire Wire Line
	5900 6350 6150 6350
Wire Wire Line
	5900 6550 6150 6550
Wire Wire Line
	6150 3650 6150 3150
Wire Wire Line
	6150 7150 6150 6650
Wire Wire Line
	6250 950  6450 950 
Wire Wire Line
	6250 4450 6450 4450
Wire Wire Line
	6450 950  6650 950 
Wire Wire Line
	6450 4450 6650 4450
Wire Wire Line
	6450 7150 6150 7150
Wire Wire Line
	6500 3350 6600 3350
Wire Wire Line
	6500 6850 6600 6850
Wire Wire Line
	6800 950  6650 950 
Wire Wire Line
	6800 4450 6650 4450
Wire Wire Line
	6950 1150 7200 1150
Wire Wire Line
	6950 1250 7200 1250
Wire Wire Line
	6950 1350 7200 1350
Wire Wire Line
	6950 1450 7200 1450
Wire Wire Line
	6950 1550 7200 1550
Wire Wire Line
	6950 1650 7200 1650
Wire Wire Line
	6950 1750 7200 1750
Wire Wire Line
	6950 1850 7200 1850
Wire Wire Line
	6950 1950 7200 1950
Wire Wire Line
	6950 2050 7200 2050
Wire Wire Line
	6950 2150 7200 2150
Wire Wire Line
	6950 2250 7200 2250
Wire Wire Line
	6950 2350 7200 2350
Wire Wire Line
	6950 2450 7200 2450
Wire Wire Line
	6950 2550 7200 2550
Wire Wire Line
	6950 2650 7200 2650
Wire Wire Line
	6950 4650 7200 4650
Wire Wire Line
	6950 4750 7200 4750
Wire Wire Line
	6950 4850 7200 4850
Wire Wire Line
	6950 4950 7200 4950
Wire Wire Line
	6950 5050 7200 5050
Wire Wire Line
	6950 5150 7200 5150
Wire Wire Line
	6950 5250 7200 5250
Wire Wire Line
	6950 5350 7200 5350
Wire Wire Line
	6950 5450 7200 5450
Wire Wire Line
	6950 5550 7200 5550
Wire Wire Line
	6950 5650 7200 5650
Wire Wire Line
	6950 5750 7200 5750
Wire Wire Line
	6950 5850 7200 5850
Wire Wire Line
	6950 5950 7200 5950
Wire Wire Line
	6950 6050 7200 6050
Wire Wire Line
	6950 6150 7200 6150
Wire Wire Line
	8400 1200 8650 1200
Wire Wire Line
	8400 1300 8650 1300
Wire Wire Line
	8400 1400 8650 1400
Wire Wire Line
	8400 1500 8650 1500
Wire Wire Line
	8400 1600 8650 1600
Wire Wire Line
	8400 1700 8650 1700
Wire Wire Line
	8400 1800 8650 1800
Wire Wire Line
	8400 1900 8650 1900
Wire Wire Line
	8400 3750 8700 3750
Wire Wire Line
	8450 5500 8700 5500
Wire Wire Line
	8700 3150 8400 3150
Wire Wire Line
	8700 3250 8400 3250
Wire Wire Line
	8700 3350 8400 3350
Wire Wire Line
	8700 3450 8400 3450
Wire Wire Line
	8700 3550 8400 3550
Wire Wire Line
	8700 3650 8400 3650
Wire Wire Line
	8700 4700 8400 4700
Wire Wire Line
	8700 4800 8400 4800
Wire Wire Line
	8700 4900 8400 4900
Wire Wire Line
	8700 5000 8400 5000
Wire Wire Line
	8700 5100 8400 5100
Wire Wire Line
	8700 5200 8400 5200
Wire Wire Line
	8700 5300 8400 5300
Wire Wire Line
	8700 5400 8400 5400
Wire Wire Line
	8700 5600 8700 5800
Wire Wire Line
	8700 5800 9200 5800
Wire Wire Line
	8850 4500 9200 4500
Wire Wire Line
	9200 4500 9500 4500
Wire Wire Line
	9300 1200 9550 1200
Wire Wire Line
	9300 1300 9550 1300
Wire Wire Line
	9300 1400 9550 1400
Wire Wire Line
	9300 1500 9550 1500
Wire Wire Line
	9300 1600 9550 1600
Wire Wire Line
	9300 1700 9550 1700
Wire Wire Line
	9300 1800 9550 1800
Wire Wire Line
	9300 1900 9550 1900
Wire Wire Line
	9350 3150 9600 3150
Wire Wire Line
	9350 3250 9600 3250
Wire Wire Line
	9350 3350 9600 3350
Wire Wire Line
	9350 3450 9600 3450
Wire Wire Line
	9350 3550 9600 3550
Wire Wire Line
	9350 3650 9600 3650
Wire Wire Line
	9350 3750 9600 3750
Wire Wire Line
	9350 3850 9600 3850
Wire Wire Line
	9700 4700 10000 4700
Wire Wire Line
	9700 4800 10000 4800
Wire Wire Line
	9700 4900 10000 4900
Wire Wire Line
	9700 5000 10000 5000
Wire Wire Line
	9700 5100 10000 5100
Wire Wire Line
	9700 5200 10000 5200
Wire Wire Line
	9700 5300 10000 5300
Wire Wire Line
	9700 5400 10000 5400
Wire Wire Line
	10250 3150 10500 3150
Wire Wire Line
	10250 3250 10500 3250
Wire Wire Line
	10250 3350 10500 3350
Wire Wire Line
	10250 3450 10500 3450
Wire Wire Line
	10250 3550 10500 3550
Wire Wire Line
	10250 3650 10500 3650
Wire Wire Line
	10250 3750 10500 3750
Wire Wire Line
	10250 3850 10500 3850
Wire Bus Line
	700  1250 700  1450
Wire Bus Line
	700  1450 700  1650
Wire Bus Line
	700  1650 700  1750
Wire Bus Line
	700  1750 700  1950
Wire Bus Line
	700  1950 700  2150
Wire Bus Line
	700  2150 700  2350
Wire Bus Line
	700  2350 700  2550
Wire Bus Line
	700  2550 700  2650
Wire Bus Line
	700  2650 700  2850
Wire Bus Line
	700  2850 700  4150
Wire Bus Line
	700  4150 700  4550
Wire Bus Line
	700  4150 2400 4150
Wire Bus Line
	700  4550 700  4750
Wire Bus Line
	700  4750 700  4950
Wire Bus Line
	700  4950 700  5050
Wire Bus Line
	700  5050 700  5250
Wire Bus Line
	700  5250 700  5450
Wire Bus Line
	700  5450 700  5650
Wire Bus Line
	700  5650 700  5750
Wire Bus Line
	700  5750 700  5950
Wire Bus Line
	700  5950 700  6150
Wire Bus Line
	700  6150 700  6250
Wire Bus Line
	2200 1250 2200 1450
Wire Bus Line
	2200 1450 2200 1550
Wire Bus Line
	2200 1550 2200 1750
Wire Bus Line
	2200 1750 2200 1950
Wire Bus Line
	2200 1950 2200 2050
Wire Bus Line
	2200 2050 2200 2250
Wire Bus Line
	2200 2250 2200 2450
Wire Bus Line
	2200 2450 2200 2650
Wire Bus Line
	2200 2650 2200 3950
Wire Bus Line
	2200 3950 2200 4650
Wire Bus Line
	2200 3950 3900 3950
Wire Bus Line
	2200 4650 2200 4750
Wire Bus Line
	2200 4750 2200 4950
Wire Bus Line
	2200 4950 2200 5150
Wire Bus Line
	2200 5150 2200 5250
Wire Bus Line
	2200 5250 2200 5450
Wire Bus Line
	2200 5450 2200 5650
Wire Bus Line
	2200 5650 2200 5850
Wire Bus Line
	2200 5850 2200 6050
Wire Bus Line
	2400 1250 2400 1350
Wire Bus Line
	2400 1350 2400 1550
Wire Bus Line
	2400 1550 2400 1750
Wire Bus Line
	2400 1750 2400 1850
Wire Bus Line
	2400 1850 2400 2050
Wire Bus Line
	2400 2050 2400 2150
Wire Bus Line
	2400 2150 2400 2350
Wire Bus Line
	2400 2350 2400 2550
Wire Bus Line
	2400 2550 2400 2750
Wire Bus Line
	2400 2750 2400 2950
Wire Bus Line
	2400 2950 2400 4150
Wire Bus Line
	2400 4150 2400 4650
Wire Bus Line
	2400 4150 4100 4150
Wire Bus Line
	2400 4650 2400 4850
Wire Bus Line
	2400 4850 2400 4950
Wire Bus Line
	2400 4950 2400 5150
Wire Bus Line
	2400 5150 2400 5350
Wire Bus Line
	2400 5350 2400 5450
Wire Bus Line
	2400 5450 2400 5650
Wire Bus Line
	2400 5650 2400 5850
Wire Bus Line
	2400 5850 2400 6050
Wire Bus Line
	2400 6050 2400 6250
Wire Bus Line
	3900 1250 3900 1450
Wire Bus Line
	3900 1450 3900 1650
Wire Bus Line
	3900 1650 3900 1850
Wire Bus Line
	3900 1850 3900 2050
Wire Bus Line
	3900 2050 3900 2250
Wire Bus Line
	3900 2250 3900 2450
Wire Bus Line
	3900 2450 3900 2550
Wire Bus Line
	3900 2550 3900 2750
Wire Bus Line
	3900 2750 3900 3950
Wire Bus Line
	3900 3950 3900 4650
Wire Bus Line
	3900 3950 5600 3950
Wire Bus Line
	3900 4650 3900 4850
Wire Bus Line
	3900 4850 3900 5050
Wire Bus Line
	3900 5050 3900 5250
Wire Bus Line
	3900 5250 3900 5450
Wire Bus Line
	3900 5450 3900 5650
Wire Bus Line
	3900 5650 3900 5750
Wire Bus Line
	3900 5750 3900 5950
Wire Bus Line
	3900 5950 3900 6050
Wire Bus Line
	4100 1250 4100 1450
Wire Bus Line
	4100 1450 4100 1550
Wire Bus Line
	4100 1550 4100 1750
Wire Bus Line
	4100 1750 4100 1950
Wire Bus Line
	4100 1950 4100 2150
Wire Bus Line
	4100 2150 4100 2350
Wire Bus Line
	4100 2350 4100 2550
Wire Bus Line
	4100 2550 4100 2650
Wire Bus Line
	4100 2650 4100 2850
Wire Bus Line
	4100 2850 4100 4150
Wire Bus Line
	4100 4150 4100 4650
Wire Bus Line
	4100 4150 5800 4150
Wire Bus Line
	4100 4650 4100 4850
Wire Bus Line
	4100 4850 4100 5050
Wire Bus Line
	4100 5050 4100 5250
Wire Bus Line
	4100 5250 4100 5450
Wire Bus Line
	4100 5450 4100 5550
Wire Bus Line
	4100 5550 4100 5750
Wire Bus Line
	4100 5750 4100 5950
Wire Bus Line
	4100 5950 4100 6150
Wire Bus Line
	4100 6150 4100 6250
Wire Bus Line
	5600 1250 5600 1350
Wire Bus Line
	5600 1350 5600 1550
Wire Bus Line
	5600 1550 5600 1750
Wire Bus Line
	5600 1750 5600 1950
Wire Bus Line
	5600 1950 5600 2150
Wire Bus Line
	5600 2150 5600 2350
Wire Bus Line
	5600 2350 5600 2550
Wire Bus Line
	5600 2550 5600 2750
Wire Bus Line
	5600 2750 5600 3950
Wire Bus Line
	5600 3950 5600 4650
Wire Bus Line
	5600 3950 7300 3950
Wire Bus Line
	5600 4650 5600 4850
Wire Bus Line
	5600 4850 5600 5050
Wire Bus Line
	5600 5050 5600 5150
Wire Bus Line
	5600 5150 5600 5350
Wire Bus Line
	5600 5350 5600 5450
Wire Bus Line
	5600 5450 5600 5650
Wire Bus Line
	5600 5650 5600 5850
Wire Bus Line
	5600 5850 5600 6050
Wire Bus Line
	5800 1250 5800 1350
Wire Bus Line
	5800 1350 5800 1550
Wire Bus Line
	5800 1550 5800 1750
Wire Bus Line
	5800 1750 5800 1950
Wire Bus Line
	5800 1950 5800 2050
Wire Bus Line
	5800 2050 5800 2250
Wire Bus Line
	5800 2250 5800 2450
Wire Bus Line
	5800 2450 5800 2550
Wire Bus Line
	5800 2550 5800 2750
Wire Bus Line
	5800 2750 5800 2950
Wire Bus Line
	5800 2950 5800 4150
Wire Bus Line
	5800 4150 5800 4650
Wire Bus Line
	5800 4150 8300 4150
Wire Bus Line
	5800 4650 5800 4850
Wire Bus Line
	5800 4850 5800 5050
Wire Bus Line
	5800 5050 5800 5250
Wire Bus Line
	5800 5250 5800 5450
Wire Bus Line
	5800 5450 5800 5650
Wire Bus Line
	5800 5650 5800 5850
Wire Bus Line
	5800 5850 5800 6050
Wire Bus Line
	5800 6050 5800 6250
Wire Bus Line
	7300 700  7300 1350
Wire Bus Line
	7300 1350 7300 1550
Wire Bus Line
	7300 1550 7300 1650
Wire Bus Line
	7300 1650 7300 1850
Wire Bus Line
	7300 1850 7300 2050
Wire Bus Line
	7300 2050 7300 2250
Wire Bus Line
	7300 2250 7300 2450
Wire Bus Line
	7300 2450 7300 2650
Wire Bus Line
	7300 2650 7300 3950
Wire Bus Line
	7300 3950 7300 4650
Wire Bus Line
	7300 4650 7300 4850
Wire Bus Line
	7300 4850 7300 5050
Wire Bus Line
	7300 5050 7300 5250
Wire Bus Line
	7300 5250 7300 5350
Wire Bus Line
	7300 5350 7300 5550
Wire Bus Line
	7300 5550 7300 5750
Wire Bus Line
	7300 5750 7300 5950
Wire Bus Line
	7300 5950 7300 6050
Wire Bus Line
	8300 700  7300 700 
Wire Bus Line
	8300 700  8300 1200
Wire Bus Line
	8300 1200 8300 1300
Wire Bus Line
	8300 1300 8300 1500
Wire Bus Line
	8300 1500 8300 1700
Wire Bus Line
	8300 1700 8300 1800
Wire Bus Line
	8300 2650 8300 3050
Wire Bus Line
	8300 2650 9250 2650
Wire Bus Line
	8300 3050 8300 3250
Wire Bus Line
	8300 3250 8300 3350
Wire Bus Line
	8300 3350 8300 3550
Wire Bus Line
	8300 3550 8300 4150
Wire Bus Line
	8300 4150 8300 4700
Wire Bus Line
	8300 4700 8300 4900
Wire Bus Line
	8300 4900 8300 5100
Wire Bus Line
	8300 5100 8300 5300
Wire Bus Line
	9200 700  8300 700 
Wire Bus Line
	9200 700  9200 1100
Wire Bus Line
	9200 700  10450 700 
Wire Bus Line
	9200 1100 9200 1300
Wire Bus Line
	9200 1300 9200 1400
Wire Bus Line
	9200 1400 9200 1600
Wire Bus Line
	9200 1600 9200 1800
Wire Bus Line
	9250 2650 9250 3150
Wire Bus Line
	9250 2650 10150 2650
Wire Bus Line
	9250 3150 9250 3250
Wire Bus Line
	9250 3250 9250 3450
Wire Bus Line
	9250 3450 9250 3550
Wire Bus Line
	9250 3550 9250 3750
Wire Bus Line
	10150 2650 10150 3050
Wire Bus Line
	10150 2650 10400 2650
Wire Bus Line
	10150 3050 10150 3250
Wire Bus Line
	10150 3250 10150 3350
Wire Bus Line
	10150 3350 10150 3550
Wire Bus Line
	10150 3550 10150 3750
Text Notes 7500 6450 0    50   ~ 0
/E3  = /E & ADDR=000000-07ffff\n/E4  = /E & ADDR=080000-0fffff\n/E5  = /E & ADDR=100000-17ffff\n/E6  = /E & ADDR=180000-1fffff\n/E7  = /E & ADDR=200000-27ffff\n/E8  = /E & ADDR=280000-2fffff\n/E9  = /E & ADDR=300000-37ffff\n/E10 = /E & ADDR=380000-3fffff
Text Notes 9250 6450 0    50   ~ 0
/o12 = /i1 & /i2 & /i3 & /i4 & /i5 & /i9\n/o13 = /i1 & /i2 & /i3 & /i4 & i5 & /i9\n/o14 = /i1 & /i2 & /i3 & i4 & /i5 & /i9\n/o15 = /i1 & /i2 & /i3 & i4 & i5 & /i9\n/o16 = /i1 & /i2 & i3 & /i4 & /i5 & /i9\n/o17 = /i1 & /i2 & i3 & /i4 & i5 & /i9\n/o18 = /i1 & /i2 & i3 & i4 & /i5 & /i9\n/o19 = /i1 & /i2 & i3 & i4 & i5 & /i9 & /i11
Text Label 850  1150 0    50   ~ 0
A1
Text Label 850  1250 0    50   ~ 0
A2
Text Label 850  1350 0    50   ~ 0
A3
Text Label 850  1450 0    50   ~ 0
A4
Text Label 850  1550 0    50   ~ 0
A5
Text Label 850  1650 0    50   ~ 0
A6
Text Label 850  1750 0    50   ~ 0
A7
Text Label 850  1850 0    50   ~ 0
A8
Text Label 850  1950 0    50   ~ 0
A9
Text Label 850  2050 0    50   ~ 0
A10
Text Label 850  2150 0    50   ~ 0
A11
Text Label 850  2250 0    50   ~ 0
A12
Text Label 850  2350 0    50   ~ 0
A13
Text Label 850  2450 0    50   ~ 0
A14
Text Label 850  2550 0    50   ~ 0
A15
Text Label 850  2650 0    50   ~ 0
A16
Text Label 850  2750 0    50   ~ 0
A17
Text Label 850  2850 0    50   ~ 0
A18
Text Label 850  3050 0    50   ~ 0
E3
Text Label 850  4650 0    50   ~ 0
A1
Text Label 850  4750 0    50   ~ 0
A2
Text Label 850  4850 0    50   ~ 0
A3
Text Label 850  4950 0    50   ~ 0
A4
Text Label 850  5050 0    50   ~ 0
A5
Text Label 850  5150 0    50   ~ 0
A6
Text Label 850  5250 0    50   ~ 0
A7
Text Label 850  5350 0    50   ~ 0
A8
Text Label 850  5450 0    50   ~ 0
A9
Text Label 850  5550 0    50   ~ 0
A10
Text Label 850  5650 0    50   ~ 0
A11
Text Label 850  5750 0    50   ~ 0
A12
Text Label 850  5850 0    50   ~ 0
A13
Text Label 850  5950 0    50   ~ 0
A14
Text Label 850  6050 0    50   ~ 0
A15
Text Label 850  6150 0    50   ~ 0
A16
Text Label 850  6250 0    50   ~ 0
A17
Text Label 850  6350 0    50   ~ 0
A18
Text Label 850  6550 0    50   ~ 0
E7
Text Label 1950 1150 0    50   ~ 0
Q0
Text Label 1950 1250 0    50   ~ 0
Q1
Text Label 1950 1350 0    50   ~ 0
Q2
Text Label 1950 1450 0    50   ~ 0
Q3
Text Label 1950 1550 0    50   ~ 0
Q4
Text Label 1950 1650 0    50   ~ 0
Q5
Text Label 1950 1750 0    50   ~ 0
Q6
Text Label 1950 1850 0    50   ~ 0
Q7
Text Label 1950 1950 0    50   ~ 0
Q8
Text Label 1950 2050 0    50   ~ 0
Q9
Text Label 1950 2150 0    50   ~ 0
Q10
Text Label 1950 2250 0    50   ~ 0
Q11
Text Label 1950 2350 0    50   ~ 0
Q12
Text Label 1950 2450 0    50   ~ 0
Q13
Text Label 1950 2550 0    50   ~ 0
Q14
Text Label 1950 2650 0    50   ~ 0
Q15
Text Label 1950 4650 0    50   ~ 0
Q0
Text Label 1950 4750 0    50   ~ 0
Q1
Text Label 1950 4850 0    50   ~ 0
Q2
Text Label 1950 4950 0    50   ~ 0
Q3
Text Label 1950 5050 0    50   ~ 0
Q4
Text Label 1950 5150 0    50   ~ 0
Q5
Text Label 1950 5250 0    50   ~ 0
Q6
Text Label 1950 5350 0    50   ~ 0
Q7
Text Label 1950 5450 0    50   ~ 0
Q8
Text Label 1950 5550 0    50   ~ 0
Q9
Text Label 1950 5650 0    50   ~ 0
Q10
Text Label 1950 5750 0    50   ~ 0
Q11
Text Label 1950 5850 0    50   ~ 0
Q12
Text Label 1950 5950 0    50   ~ 0
Q13
Text Label 1950 6050 0    50   ~ 0
Q14
Text Label 1950 6150 0    50   ~ 0
Q15
Text Label 2550 1150 0    50   ~ 0
A1
Text Label 2550 1250 0    50   ~ 0
A2
Text Label 2550 1350 0    50   ~ 0
A3
Text Label 2550 1450 0    50   ~ 0
A4
Text Label 2550 1550 0    50   ~ 0
A5
Text Label 2550 1650 0    50   ~ 0
A6
Text Label 2550 1750 0    50   ~ 0
A7
Text Label 2550 1850 0    50   ~ 0
A8
Text Label 2550 1950 0    50   ~ 0
A9
Text Label 2550 2050 0    50   ~ 0
A10
Text Label 2550 2150 0    50   ~ 0
A11
Text Label 2550 2250 0    50   ~ 0
A12
Text Label 2550 2350 0    50   ~ 0
A13
Text Label 2550 2450 0    50   ~ 0
A14
Text Label 2550 2550 0    50   ~ 0
A15
Text Label 2550 2650 0    50   ~ 0
A16
Text Label 2550 2750 0    50   ~ 0
A17
Text Label 2550 2850 0    50   ~ 0
A18
Text Label 2550 3050 0    50   ~ 0
E4
Text Label 2550 4650 0    50   ~ 0
A1
Text Label 2550 4750 0    50   ~ 0
A2
Text Label 2550 4850 0    50   ~ 0
A3
Text Label 2550 4950 0    50   ~ 0
A4
Text Label 2550 5050 0    50   ~ 0
A5
Text Label 2550 5150 0    50   ~ 0
A6
Text Label 2550 5250 0    50   ~ 0
A7
Text Label 2550 5350 0    50   ~ 0
A8
Text Label 2550 5450 0    50   ~ 0
A9
Text Label 2550 5550 0    50   ~ 0
A10
Text Label 2550 5650 0    50   ~ 0
A11
Text Label 2550 5750 0    50   ~ 0
A12
Text Label 2550 5850 0    50   ~ 0
A13
Text Label 2550 5950 0    50   ~ 0
A14
Text Label 2550 6050 0    50   ~ 0
A15
Text Label 2550 6150 0    50   ~ 0
A16
Text Label 2550 6250 0    50   ~ 0
A17
Text Label 2550 6350 0    50   ~ 0
A18
Text Label 2550 6550 0    50   ~ 0
E8
Text Label 3650 1150 0    50   ~ 0
Q0
Text Label 3650 1250 0    50   ~ 0
Q1
Text Label 3650 1350 0    50   ~ 0
Q2
Text Label 3650 1450 0    50   ~ 0
Q3
Text Label 3650 1550 0    50   ~ 0
Q4
Text Label 3650 1650 0    50   ~ 0
Q5
Text Label 3650 1750 0    50   ~ 0
Q6
Text Label 3650 1850 0    50   ~ 0
Q7
Text Label 3650 1950 0    50   ~ 0
Q8
Text Label 3650 2050 0    50   ~ 0
Q9
Text Label 3650 2150 0    50   ~ 0
Q10
Text Label 3650 2250 0    50   ~ 0
Q11
Text Label 3650 2350 0    50   ~ 0
Q12
Text Label 3650 2450 0    50   ~ 0
Q13
Text Label 3650 2550 0    50   ~ 0
Q14
Text Label 3650 2650 0    50   ~ 0
Q15
Text Label 3650 4650 0    50   ~ 0
Q0
Text Label 3650 4750 0    50   ~ 0
Q1
Text Label 3650 4850 0    50   ~ 0
Q2
Text Label 3650 4950 0    50   ~ 0
Q3
Text Label 3650 5050 0    50   ~ 0
Q4
Text Label 3650 5150 0    50   ~ 0
Q5
Text Label 3650 5250 0    50   ~ 0
Q6
Text Label 3650 5350 0    50   ~ 0
Q7
Text Label 3650 5450 0    50   ~ 0
Q8
Text Label 3650 5550 0    50   ~ 0
Q9
Text Label 3650 5650 0    50   ~ 0
Q10
Text Label 3650 5750 0    50   ~ 0
Q11
Text Label 3650 5850 0    50   ~ 0
Q12
Text Label 3650 5950 0    50   ~ 0
Q13
Text Label 3650 6050 0    50   ~ 0
Q14
Text Label 3650 6150 0    50   ~ 0
Q15
Text Label 4250 1150 0    50   ~ 0
A1
Text Label 4250 1250 0    50   ~ 0
A2
Text Label 4250 1350 0    50   ~ 0
A3
Text Label 4250 1450 0    50   ~ 0
A4
Text Label 4250 1550 0    50   ~ 0
A5
Text Label 4250 1650 0    50   ~ 0
A6
Text Label 4250 1750 0    50   ~ 0
A7
Text Label 4250 1850 0    50   ~ 0
A8
Text Label 4250 1950 0    50   ~ 0
A9
Text Label 4250 2050 0    50   ~ 0
A10
Text Label 4250 2150 0    50   ~ 0
A11
Text Label 4250 2250 0    50   ~ 0
A12
Text Label 4250 2350 0    50   ~ 0
A13
Text Label 4250 2450 0    50   ~ 0
A14
Text Label 4250 2550 0    50   ~ 0
A15
Text Label 4250 2650 0    50   ~ 0
A16
Text Label 4250 2750 0    50   ~ 0
A17
Text Label 4250 2850 0    50   ~ 0
A18
Text Label 4250 3050 0    50   ~ 0
E5
Text Label 4250 4650 0    50   ~ 0
A1
Text Label 4250 4750 0    50   ~ 0
A2
Text Label 4250 4850 0    50   ~ 0
A3
Text Label 4250 4950 0    50   ~ 0
A4
Text Label 4250 5050 0    50   ~ 0
A5
Text Label 4250 5150 0    50   ~ 0
A6
Text Label 4250 5250 0    50   ~ 0
A7
Text Label 4250 5350 0    50   ~ 0
A8
Text Label 4250 5450 0    50   ~ 0
A9
Text Label 4250 5550 0    50   ~ 0
A10
Text Label 4250 5650 0    50   ~ 0
A11
Text Label 4250 5750 0    50   ~ 0
A12
Text Label 4250 5850 0    50   ~ 0
A13
Text Label 4250 5950 0    50   ~ 0
A14
Text Label 4250 6050 0    50   ~ 0
A15
Text Label 4250 6150 0    50   ~ 0
A16
Text Label 4250 6250 0    50   ~ 0
A17
Text Label 4250 6350 0    50   ~ 0
A18
Text Label 4250 6550 0    50   ~ 0
E9
Text Label 5350 1150 0    50   ~ 0
Q0
Text Label 5350 1250 0    50   ~ 0
Q1
Text Label 5350 1350 0    50   ~ 0
Q2
Text Label 5350 1450 0    50   ~ 0
Q3
Text Label 5350 1550 0    50   ~ 0
Q4
Text Label 5350 1650 0    50   ~ 0
Q5
Text Label 5350 1750 0    50   ~ 0
Q6
Text Label 5350 1850 0    50   ~ 0
Q7
Text Label 5350 1950 0    50   ~ 0
Q8
Text Label 5350 2050 0    50   ~ 0
Q9
Text Label 5350 2150 0    50   ~ 0
Q10
Text Label 5350 2250 0    50   ~ 0
Q11
Text Label 5350 2350 0    50   ~ 0
Q12
Text Label 5350 2450 0    50   ~ 0
Q13
Text Label 5350 2550 0    50   ~ 0
Q14
Text Label 5350 2650 0    50   ~ 0
Q15
Text Label 5350 4650 0    50   ~ 0
Q0
Text Label 5350 4750 0    50   ~ 0
Q1
Text Label 5350 4850 0    50   ~ 0
Q2
Text Label 5350 4950 0    50   ~ 0
Q3
Text Label 5350 5050 0    50   ~ 0
Q4
Text Label 5350 5150 0    50   ~ 0
Q5
Text Label 5350 5250 0    50   ~ 0
Q6
Text Label 5350 5350 0    50   ~ 0
Q7
Text Label 5350 5450 0    50   ~ 0
Q8
Text Label 5350 5550 0    50   ~ 0
Q9
Text Label 5350 5650 0    50   ~ 0
Q10
Text Label 5350 5750 0    50   ~ 0
Q11
Text Label 5350 5850 0    50   ~ 0
Q12
Text Label 5350 5950 0    50   ~ 0
Q13
Text Label 5350 6050 0    50   ~ 0
Q14
Text Label 5350 6150 0    50   ~ 0
Q15
Text Label 5950 1150 0    50   ~ 0
A1
Text Label 5950 1250 0    50   ~ 0
A2
Text Label 5950 1350 0    50   ~ 0
A3
Text Label 5950 1450 0    50   ~ 0
A4
Text Label 5950 1550 0    50   ~ 0
A5
Text Label 5950 1650 0    50   ~ 0
A6
Text Label 5950 1750 0    50   ~ 0
A7
Text Label 5950 1850 0    50   ~ 0
A8
Text Label 5950 1950 0    50   ~ 0
A9
Text Label 5950 2050 0    50   ~ 0
A10
Text Label 5950 2150 0    50   ~ 0
A11
Text Label 5950 2250 0    50   ~ 0
A12
Text Label 5950 2350 0    50   ~ 0
A13
Text Label 5950 2450 0    50   ~ 0
A14
Text Label 5950 2550 0    50   ~ 0
A15
Text Label 5950 2650 0    50   ~ 0
A16
Text Label 5950 2750 0    50   ~ 0
A17
Text Label 5950 2850 0    50   ~ 0
A18
Text Label 5950 3050 0    50   ~ 0
E6
Text Label 5950 4650 0    50   ~ 0
A1
Text Label 5950 4750 0    50   ~ 0
A2
Text Label 5950 4850 0    50   ~ 0
A3
Text Label 5950 4950 0    50   ~ 0
A4
Text Label 5950 5050 0    50   ~ 0
A5
Text Label 5950 5150 0    50   ~ 0
A6
Text Label 5950 5250 0    50   ~ 0
A7
Text Label 5950 5350 0    50   ~ 0
A8
Text Label 5950 5450 0    50   ~ 0
A9
Text Label 5950 5550 0    50   ~ 0
A10
Text Label 5950 5650 0    50   ~ 0
A11
Text Label 5950 5750 0    50   ~ 0
A12
Text Label 5950 5850 0    50   ~ 0
A13
Text Label 5950 5950 0    50   ~ 0
A14
Text Label 5950 6050 0    50   ~ 0
A15
Text Label 5950 6150 0    50   ~ 0
A16
Text Label 5950 6250 0    50   ~ 0
A17
Text Label 5950 6350 0    50   ~ 0
A18
Text Label 5950 6550 0    50   ~ 0
E10
Text Label 7050 1150 0    50   ~ 0
Q0
Text Label 7050 1250 0    50   ~ 0
Q1
Text Label 7050 1350 0    50   ~ 0
Q2
Text Label 7050 1450 0    50   ~ 0
Q3
Text Label 7050 1550 0    50   ~ 0
Q4
Text Label 7050 1650 0    50   ~ 0
Q5
Text Label 7050 1750 0    50   ~ 0
Q6
Text Label 7050 1850 0    50   ~ 0
Q7
Text Label 7050 1950 0    50   ~ 0
Q8
Text Label 7050 2050 0    50   ~ 0
Q9
Text Label 7050 2150 0    50   ~ 0
Q10
Text Label 7050 2250 0    50   ~ 0
Q11
Text Label 7050 2350 0    50   ~ 0
Q12
Text Label 7050 2450 0    50   ~ 0
Q13
Text Label 7050 2550 0    50   ~ 0
Q14
Text Label 7050 2650 0    50   ~ 0
Q15
Text Label 7050 4650 0    50   ~ 0
Q0
Text Label 7050 4750 0    50   ~ 0
Q1
Text Label 7050 4850 0    50   ~ 0
Q2
Text Label 7050 4950 0    50   ~ 0
Q3
Text Label 7050 5050 0    50   ~ 0
Q4
Text Label 7050 5150 0    50   ~ 0
Q5
Text Label 7050 5250 0    50   ~ 0
Q6
Text Label 7050 5350 0    50   ~ 0
Q7
Text Label 7050 5450 0    50   ~ 0
Q8
Text Label 7050 5550 0    50   ~ 0
Q9
Text Label 7050 5650 0    50   ~ 0
Q10
Text Label 7050 5750 0    50   ~ 0
Q11
Text Label 7050 5850 0    50   ~ 0
Q12
Text Label 7050 5950 0    50   ~ 0
Q13
Text Label 7050 6050 0    50   ~ 0
Q14
Text Label 7050 6150 0    50   ~ 0
Q15
Text Label 8450 3150 0    50   ~ 0
A17
Text Label 8450 3250 0    50   ~ 0
A18
Text Label 8450 3350 0    50   ~ 0
A19
Text Label 8450 3450 0    50   ~ 0
A20
Text Label 8450 3550 0    50   ~ 0
A21
Text Label 8450 3650 0    50   ~ 0
A22
Text Label 8450 3750 0    50   ~ 0
A23
Text Label 8450 4700 0    50   ~ 0
A23
Text Label 8450 4800 0    50   ~ 0
A22
Text Label 8450 4900 0    50   ~ 0
A21
Text Label 8450 5000 0    50   ~ 0
A20
Text Label 8450 5100 0    50   ~ 0
A19
Text Label 8450 5200 0    50   ~ 0
A18
Text Label 8450 5300 0    50   ~ 0
A17
Text Label 8450 5400 0    50   ~ 0
A16
Text Label 8500 1200 0    50   ~ 0
Q0
Text Label 8500 1300 0    50   ~ 0
Q1
Text Label 8500 1400 0    50   ~ 0
Q2
Text Label 8500 1500 0    50   ~ 0
Q3
Text Label 8500 1600 0    50   ~ 0
Q4
Text Label 8500 1700 0    50   ~ 0
Q5
Text Label 8500 1800 0    50   ~ 0
Q6
Text Label 8500 1900 0    50   ~ 0
Q7
Text Label 9400 700  0    50   ~ 0
Q[0..15]
Text Label 9400 1200 0    50   ~ 0
Q8
Text Label 9400 1300 0    50   ~ 0
Q9
Text Label 9400 1400 0    50   ~ 0
Q10
Text Label 9400 1500 0    50   ~ 0
Q11
Text Label 9400 1600 0    50   ~ 0
Q12
Text Label 9400 1700 0    50   ~ 0
Q13
Text Label 9400 1800 0    50   ~ 0
Q14
Text Label 9400 1900 0    50   ~ 0
Q15
Text Label 9400 3150 0    50   ~ 0
A9
Text Label 9400 3250 0    50   ~ 0
A10
Text Label 9400 3350 0    50   ~ 0
A11
Text Label 9400 3450 0    50   ~ 0
A12
Text Label 9400 3550 0    50   ~ 0
A13
Text Label 9400 3650 0    50   ~ 0
A14
Text Label 9400 3750 0    50   ~ 0
A15
Text Label 9400 3850 0    50   ~ 0
A16
Text Label 9850 4700 0    50   ~ 0
E10
Text Label 9850 4800 0    50   ~ 0
E9
Text Label 9850 4900 0    50   ~ 0
E8
Text Label 9850 5000 0    50   ~ 0
E7
Text Label 9850 5100 0    50   ~ 0
E6
Text Label 9850 5200 0    50   ~ 0
E5
Text Label 9850 5300 0    50   ~ 0
E4
Text Label 9850 5400 0    50   ~ 0
E3
Text Label 10250 2650 2    50   ~ 0
A[1..23]
Text Label 10300 3150 0    50   ~ 0
A1
Text Label 10300 3250 0    50   ~ 0
A2
Text Label 10300 3350 0    50   ~ 0
A3
Text Label 10300 3450 0    50   ~ 0
A4
Text Label 10300 3550 0    50   ~ 0
A5
Text Label 10300 3650 0    50   ~ 0
A6
Text Label 10300 3750 0    50   ~ 0
A7
Text Label 10300 3850 0    50   ~ 0
A8
Text HLabel 6450 7150 2    50   Input ~ 0
~OE
Text HLabel 8450 5500 0    50   Input ~ 0
~E
Text HLabel 10400 2650 2    50   Input ~ 0
ADDR[1..23]
Text HLabel 10450 700  2    50   Output ~ 0
DATA[0..15]
$Comp
L power:VCC #PWR07
U 1 1 5FB286E3
P 1150 950
F 0 "#PWR07" H 1150 800 50  0001 C CNN
F 1 "VCC" H 950 1050 50  0000 L CNN
F 2 "" H 1150 950 50  0001 C CNN
F 3 "" H 1150 950 50  0001 C CNN
	1    1150 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 5DAA89AD
P 1150 4450
F 0 "#PWR0110" H 1150 4300 50  0001 C CNN
F 1 "VCC" H 950 4550 50  0000 L CNN
F 2 "" H 1150 4450 50  0001 C CNN
F 3 "" H 1150 4450 50  0001 C CNN
	1    1150 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR08
U 1 1 5FB2870A
P 2850 950
F 0 "#PWR08" H 2850 800 50  0001 C CNN
F 1 "VCC" H 2650 1050 50  0000 L CNN
F 2 "" H 2850 950 50  0001 C CNN
F 3 "" H 2850 950 50  0001 C CNN
	1    2850 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR09
U 1 1 5FAD4D13
P 2850 4450
F 0 "#PWR09" H 2850 4300 50  0001 C CNN
F 1 "VCC" H 2650 4550 50  0000 L CNN
F 2 "" H 2850 4450 50  0001 C CNN
F 3 "" H 2850 4450 50  0001 C CNN
	1    2850 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR010
U 1 1 5FB28731
P 4550 950
F 0 "#PWR010" H 4550 800 50  0001 C CNN
F 1 "VCC" H 4350 1050 50  0000 L CNN
F 2 "" H 4550 950 50  0001 C CNN
F 3 "" H 4550 950 50  0001 C CNN
	1    4550 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR011
U 1 1 5FB287B3
P 4550 4450
F 0 "#PWR011" H 4550 4300 50  0001 C CNN
F 1 "VCC" H 4350 4550 50  0000 L CNN
F 2 "" H 4550 4450 50  0001 C CNN
F 3 "" H 4550 4450 50  0001 C CNN
	1    4550 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR012
U 1 1 5FB2878C
P 6250 950
F 0 "#PWR012" H 6250 800 50  0001 C CNN
F 1 "VCC" H 6050 1050 50  0000 L CNN
F 2 "" H 6250 950 50  0001 C CNN
F 3 "" H 6250 950 50  0001 C CNN
	1    6250 950 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR013
U 1 1 5FB287DA
P 6250 4450
F 0 "#PWR013" H 6250 4300 50  0001 C CNN
F 1 "VCC" H 6050 4550 50  0000 L CNN
F 2 "" H 6250 4450 50  0001 C CNN
F 3 "" H 6250 4450 50  0001 C CNN
	1    6250 4450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR014
U 1 1 5FBD0955
P 8850 4500
F 0 "#PWR014" H 8850 4350 50  0001 C CNN
F 1 "VCC" H 8650 4600 50  0000 L CNN
F 2 "" H 8850 4500 50  0001 C CNN
F 3 "" H 8850 4500 50  0001 C CNN
	1    8850 4500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0134
U 1 1 624EC4DD
P 9050 1200
F 0 "#PWR0134" H 9050 1050 50  0001 C CNN
F 1 "VCC" H 9067 1373 50  0000 C CNN
F 2 "" H 9050 1200 50  0001 C CNN
F 3 "" H 9050 1200 50  0001 C CNN
	1    9050 1200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0133
U 1 1 62226945
P 9100 3150
F 0 "#PWR0133" H 9100 3000 50  0001 C CNN
F 1 "VCC" H 9117 3323 50  0000 C CNN
F 2 "" H 9100 3150 50  0001 C CNN
F 3 "" H 9100 3150 50  0001 C CNN
	1    9100 3150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0135
U 1 1 6253B16E
P 9950 1200
F 0 "#PWR0135" H 9950 1050 50  0001 C CNN
F 1 "VCC" H 9967 1373 50  0000 C CNN
F 2 "" H 9950 1200 50  0001 C CNN
F 3 "" H 9950 1200 50  0001 C CNN
	1    9950 1200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0136
U 1 1 62952761
P 10000 3150
F 0 "#PWR0136" H 10000 3000 50  0001 C CNN
F 1 "VCC" H 10017 3323 50  0000 C CNN
F 2 "" H 10000 3150 50  0001 C CNN
F 3 "" H 10000 3150 50  0001 C CNN
	1    10000 3150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0137
U 1 1 62A0520D
P 10900 3150
F 0 "#PWR0137" H 10900 3000 50  0001 C CNN
F 1 "VCC" H 10917 3323 50  0000 C CNN
F 2 "" H 10900 3150 50  0001 C CNN
F 3 "" H 10900 3150 50  0001 C CNN
	1    10900 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0131
U 1 1 5DEAD256
P 1500 3350
F 0 "#PWR0131" H 1500 3100 50  0001 C CNN
F 1 "GND" H 1505 3177 50  0001 C CNN
F 2 "" H 1500 3350 50  0001 C CNN
F 3 "" H 1500 3350 50  0001 C CNN
	1    1500 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0130
U 1 1 5E14C465
P 1500 6850
F 0 "#PWR0130" H 1500 6600 50  0001 C CNN
F 1 "GND" H 1505 6677 50  0001 C CNN
F 2 "" H 1500 6850 50  0001 C CNN
F 3 "" H 1500 6850 50  0001 C CNN
	1    1500 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5E7C65C3
P 1900 950
F 0 "#PWR0116" H 1900 700 50  0001 C CNN
F 1 "GND" H 1905 777 50  0001 C CNN
F 2 "" H 1900 950 50  0001 C CNN
F 3 "" H 1900 950 50  0001 C CNN
	1    1900 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5EA8FB8F
P 1900 4450
F 0 "#PWR0121" H 1900 4200 50  0001 C CNN
F 1 "GND" H 1905 4277 50  0001 C CNN
F 2 "" H 1900 4450 50  0001 C CNN
F 3 "" H 1900 4450 50  0001 C CNN
	1    1900 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0132
U 1 1 5DF55670
P 3200 3350
F 0 "#PWR0132" H 3200 3100 50  0001 C CNN
F 1 "GND" H 3205 3177 50  0001 C CNN
F 2 "" H 3200 3350 50  0001 C CNN
F 3 "" H 3200 3350 50  0001 C CNN
	1    3200 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0129
U 1 1 5E1BBE88
P 3200 6850
F 0 "#PWR0129" H 3200 6600 50  0001 C CNN
F 1 "GND" H 3205 6677 50  0001 C CNN
F 2 "" H 3200 6850 50  0001 C CNN
F 3 "" H 3200 6850 50  0001 C CNN
	1    3200 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5E9DA321
P 3600 950
F 0 "#PWR0119" H 3600 700 50  0001 C CNN
F 1 "GND" H 3605 777 50  0001 C CNN
F 2 "" H 3600 950 50  0001 C CNN
F 3 "" H 3600 950 50  0001 C CNN
	1    3600 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5E99E4B5
P 3600 4450
F 0 "#PWR0118" H 3600 4200 50  0001 C CNN
F 1 "GND" H 3605 4277 50  0001 C CNN
F 2 "" H 3600 4450 50  0001 C CNN
F 3 "" H 3600 4450 50  0001 C CNN
	1    3600 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5DFC5089
P 4900 3350
F 0 "#PWR0114" H 4900 3100 50  0001 C CNN
F 1 "GND" H 4905 3177 50  0001 C CNN
F 2 "" H 4900 3350 50  0001 C CNN
F 3 "" H 4900 3350 50  0001 C CNN
	1    4900 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5E22B8AB
P 4900 6850
F 0 "#PWR0128" H 4900 6600 50  0001 C CNN
F 1 "GND" H 4905 6677 50  0001 C CNN
F 2 "" H 4900 6850 50  0001 C CNN
F 3 "" H 4900 6850 50  0001 C CNN
	1    4900 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5E927607
P 5300 950
F 0 "#PWR0117" H 5300 700 50  0001 C CNN
F 1 "GND" H 5305 777 50  0001 C CNN
F 2 "" H 5300 950 50  0001 C CNN
F 3 "" H 5300 950 50  0001 C CNN
	1    5300 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5EB840D6
P 5300 4450
F 0 "#PWR0123" H 5300 4200 50  0001 C CNN
F 1 "GND" H 5305 4277 50  0001 C CNN
F 2 "" H 5300 4450 50  0001 C CNN
F 3 "" H 5300 4450 50  0001 C CNN
	1    5300 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5E034AF0
P 6600 3350
F 0 "#PWR0115" H 6600 3100 50  0001 C CNN
F 1 "GND" H 6605 3177 50  0001 C CNN
F 2 "" H 6600 3350 50  0001 C CNN
F 3 "" H 6600 3350 50  0001 C CNN
	1    6600 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5E0A4541
P 6600 6850
F 0 "#PWR0127" H 6600 6600 50  0001 C CNN
F 1 "GND" H 6605 6677 50  0001 C CNN
F 2 "" H 6600 6850 50  0001 C CNN
F 3 "" H 6600 6850 50  0001 C CNN
	1    6600 6850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5EA52E8D
P 7000 950
F 0 "#PWR0120" H 7000 700 50  0001 C CNN
F 1 "GND" H 7005 777 50  0001 C CNN
F 2 "" H 7000 950 50  0001 C CNN
F 3 "" H 7000 950 50  0001 C CNN
	1    7000 950 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5EB840C9
P 7000 4450
F 0 "#PWR0122" H 7000 4200 50  0001 C CNN
F 1 "GND" H 7005 4277 50  0001 C CNN
F 2 "" H 7000 4450 50  0001 C CNN
F 3 "" H 7000 4450 50  0001 C CNN
	1    7000 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 5FB18C40
P 9200 5800
F 0 "#PWR0124" H 9200 5550 50  0001 C CNN
F 1 "GND" H 9205 5627 50  0001 C CNN
F 2 "" H 9200 5800 50  0001 C CNN
F 3 "" H 9200 5800 50  0001 C CNN
	1    9200 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0125
U 1 1 5FE273E5
P 9700 4500
F 0 "#PWR0125" H 9700 4250 50  0001 C CNN
F 1 "GND" H 9705 4327 50  0001 C CNN
F 2 "" H 9700 4500 50  0001 C CNN
F 3 "" H 9700 4500 50  0001 C CNN
	1    9700 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small CX1
U 1 1 5E7C65BD
P 1800 950
F 0 "CX1" V 1571 950 50  0001 C CNN
F 1 "0.1u" V 1662 950 50  0000 C TNN
F 2 "" H 1800 950 50  0001 C CNN
F 3 "~" H 1800 950 50  0001 C CNN
	1    1800 950 
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX2
U 1 1 5EA8FB89
P 1800 4450
F 0 "CX2" V 1571 4450 50  0001 C CNN
F 1 "0.1u" V 1662 4450 50  0000 C TNN
F 2 "" H 1800 4450 50  0001 C CNN
F 3 "~" H 1800 4450 50  0001 C CNN
	1    1800 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX3
U 1 1 5E9DA31B
P 3500 950
F 0 "CX3" V 3271 950 50  0001 C CNN
F 1 "0.1u" V 3362 950 50  0000 C TNN
F 2 "" H 3500 950 50  0001 C CNN
F 3 "~" H 3500 950 50  0001 C CNN
	1    3500 950 
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX4
U 1 1 5E99E4AF
P 3500 4450
F 0 "CX4" V 3271 4450 50  0001 C CNN
F 1 "0.1u" V 3362 4450 50  0000 C TNN
F 2 "" H 3500 4450 50  0001 C CNN
F 3 "~" H 3500 4450 50  0001 C CNN
	1    3500 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX5
U 1 1 5E927601
P 5200 950
F 0 "CX5" V 4971 950 50  0001 C CNN
F 1 "0.1u" V 5062 950 50  0000 C TNN
F 2 "" H 5200 950 50  0001 C CNN
F 3 "~" H 5200 950 50  0001 C CNN
	1    5200 950 
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX6
U 1 1 5EB840D0
P 5200 4450
F 0 "CX6" V 4971 4450 50  0001 C CNN
F 1 "0.1u" V 5062 4450 50  0000 C TNN
F 2 "" H 5200 4450 50  0001 C CNN
F 3 "~" H 5200 4450 50  0001 C CNN
	1    5200 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX7
U 1 1 5EA52E87
P 6900 950
F 0 "CX7" V 6671 950 50  0001 C CNN
F 1 "0.1u" V 6762 950 50  0000 C TNN
F 2 "" H 6900 950 50  0001 C CNN
F 3 "~" H 6900 950 50  0001 C CNN
	1    6900 950 
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX8
U 1 1 5EB840C3
P 6900 4450
F 0 "CX8" V 6671 4450 50  0001 C CNN
F 1 "0.1u" V 6762 4450 50  0000 C TNN
F 2 "" H 6900 4450 50  0001 C CNN
F 3 "~" H 6900 4450 50  0001 C CNN
	1    6900 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small CX9
U 1 1 5FDE60B7
P 9600 4500
F 0 "CX9" V 9371 4500 50  0001 C CNN
F 1 "0.1u" V 9462 4500 50  0000 C TNN
F 2 "" H 9600 4500 50  0001 C CNN
F 3 "~" H 9600 4500 50  0001 C CNN
	1    9600 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R_Network08 RA9
U 1 1 61B22E4A
P 8850 1600
F 0 "RA9" V 8233 1600 50  0000 C CNN
F 1 "4.7k" V 8324 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 9325 1600 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 8850 1600 50  0001 C CNN
	1    8850 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R_Network08 RA25
U 1 1 61F2E205
P 8900 3550
F 0 "RA25" V 8283 3550 50  0000 C CNN
F 1 "4.7k" V 8374 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 9375 3550 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 8900 3550 50  0001 C CNN
	1    8900 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Network08 RA3
U 1 1 61B6C8D6
P 9750 1600
F 0 "RA3" V 9133 1600 50  0000 C CNN
F 1 "4.7k" V 9224 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 10225 1600 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9750 1600 50  0001 C CNN
	1    9750 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R_Network08 RA12
U 1 1 61AD9265
P 9800 3550
F 0 "RA12" V 9183 3550 50  0000 C CNN
F 1 "4.7k" V 9274 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 10275 3550 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9800 3550 50  0001 C CNN
	1    9800 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R_Network08 RA15
U 1 1 61AD93C3
P 10700 3550
F 0 "RA15" V 10083 3550 50  0000 C CNN
F 1 "4.7k" V 10174 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Array_SIP9" V 11175 3550 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 10700 3550 50  0001 C CNN
	1    10700 3550
	0    1    1    0   
$EndComp
$Comp
L Logic_Programmable:PAL16L8 UH9
U 1 1 5ECF5DA2
P 9200 5200
F 0 "UH9" H 9200 6078 50  0000 C CNN
F 1 "PAL16L8" H 9200 5987 50  0000 C CNN
F 2 "" H 9200 5200 50  0001 C CNN
F 3 "" H 9200 5200 50  0001 C CNN
	1    9200 5200
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UA6-ROM3
U 1 1 5D957D26
P 1450 2150
F 0 "UA6-ROM3" H 1450 3528 50  0000 C CNN
F 1 "27C4096" H 1450 3437 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 1500 2150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 1450 2250 50  0001 C CNN
	1    1450 2150
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UD6-ROM7
U 1 1 5DAA89A5
P 1450 5650
F 0 "UD6-ROM7" H 1450 7028 50  0000 C CNN
F 1 "27C4096" H 1450 6937 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 1500 5650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 1450 5750 50  0001 C CNN
	1    1450 5650
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UA7-ROM4
U 1 1 5D95981C
P 3150 2150
F 0 "UA7-ROM4" H 3150 3528 50  0000 C CNN
F 1 "27C4096" H 3150 3437 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 3200 2150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 3150 2250 50  0001 C CNN
	1    3150 2150
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UD7-ROM8
U 1 1 5DAA89BE
P 3150 5650
F 0 "UD7-ROM8" H 3150 7028 50  0000 C CNN
F 1 "27C4096" H 3150 6937 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 3200 5650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 3150 5750 50  0001 C CNN
	1    3150 5650
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UA8-ROM5
U 1 1 5D996ED1
P 4850 2150
F 0 "UA8-ROM5" H 4850 3528 50  0000 C CNN
F 1 "27C4096" H 4850 3437 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 4900 2150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 4850 2250 50  0001 C CNN
	1    4850 2150
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UD8-ROM9
U 1 1 5DAA8AA5
P 4850 5650
F 0 "UD8-ROM9" H 4850 7028 50  0000 C CNN
F 1 "27C4096" H 4850 6937 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 4900 5650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 4850 5750 50  0001 C CNN
	1    4850 5650
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UA9-ROM6
U 1 1 5D996EEA
P 6550 2150
F 0 "UA9-ROM6" H 6550 3528 50  0000 C CNN
F 1 "27C4096" H 6550 3437 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 6600 2150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 6550 2250 50  0001 C CNN
	1    6550 2150
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4096 UD9-ROM10
U 1 1 5DAA8ABE
P 6550 5650
F 0 "UD9-ROM10" H 6550 7028 50  0000 C CNN
F 1 "27C4096" H 6550 6937 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 6600 5650 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 6550 5750 50  0001 C CNN
	1    6550 5650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
