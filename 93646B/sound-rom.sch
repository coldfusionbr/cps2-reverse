EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 17
Title ""
Date "2019-10-17"
Rev ""
Comp ""
Comment1 ""
Comment2 "Licence: CC-BY"
Comment3 "Author: Loic *WydD* Petit"
Comment4 "Project: CPS2 Reverse Engineering: 93646B"
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 3200 2300
Connection ~ 3200 2700
Connection ~ 3650 3100
Connection ~ 3550 2600
Connection ~ 4550 3050
Connection ~ 4550 6000
Connection ~ 3550 3750
Connection ~ 3200 3850
Connection ~ 3200 3450
Connection ~ 4950 4000
Connection ~ 4950 1050
Connection ~ 5800 3700
Connection ~ 3550 3350
Connection ~ 3650 3550
Connection ~ 3650 2800
Entry Wire Line
	4100 1350 4200 1250
Entry Wire Line
	4100 1450 4200 1350
Entry Wire Line
	4100 1550 4200 1450
Entry Wire Line
	4100 1650 4200 1550
Entry Wire Line
	4100 1750 4200 1650
Entry Wire Line
	4100 1850 4200 1750
Entry Wire Line
	4100 1950 4200 1850
Entry Wire Line
	4100 2050 4200 1950
Entry Wire Line
	4100 2150 4200 2050
Entry Wire Line
	4100 2250 4200 2150
Entry Wire Line
	4100 2350 4200 2250
Entry Wire Line
	4100 2450 4200 2350
Entry Wire Line
	4100 2550 4200 2450
Entry Wire Line
	4100 2650 4200 2550
Entry Wire Line
	4100 2750 4200 2650
Entry Wire Line
	4100 2850 4200 2750
Entry Wire Line
	4100 3000 4000 3100
Entry Wire Line
	4100 4100 4200 4200
Entry Wire Line
	4100 4200 4200 4300
Entry Wire Line
	4100 4300 4200 4400
Entry Wire Line
	4100 4400 4200 4500
Entry Wire Line
	4100 4500 4200 4600
Entry Wire Line
	4100 4600 4200 4700
Entry Wire Line
	4100 4700 4200 4800
Entry Wire Line
	4100 4800 4200 4900
Entry Wire Line
	4100 4900 4200 5000
Entry Wire Line
	4100 5000 4200 5100
Entry Wire Line
	4100 5100 4200 5200
Entry Wire Line
	4100 5200 4200 5300
Entry Wire Line
	4100 5300 4200 5400
Entry Wire Line
	4100 5400 4200 5500
Entry Wire Line
	4100 5500 4200 5600
Entry Wire Line
	4100 5600 4200 5700
Entry Wire Line
	5700 1250 5800 1350
Entry Wire Line
	5700 1350 5800 1450
Entry Wire Line
	5700 1450 5800 1550
Entry Wire Line
	5700 1550 5800 1650
Entry Wire Line
	5700 1650 5800 1750
Entry Wire Line
	5700 1750 5800 1850
Entry Wire Line
	5700 1850 5800 1950
Entry Wire Line
	5700 1950 5800 2050
Entry Wire Line
	5700 4200 5800 4100
Entry Wire Line
	5700 4300 5800 4200
Entry Wire Line
	5700 4400 5800 4300
Entry Wire Line
	5700 4500 5800 4400
Entry Wire Line
	5700 4600 5800 4500
Entry Wire Line
	5700 4700 5800 4600
Entry Wire Line
	5700 4800 5800 4700
Entry Wire Line
	5700 4900 5800 4800
Wire Wire Line
	3200 2200 3200 2300
Wire Wire Line
	3200 2300 2850 2300
Wire Wire Line
	3200 2300 3200 2400
Wire Wire Line
	3200 2600 3200 2700
Wire Wire Line
	3200 2700 2850 2700
Wire Wire Line
	3200 2700 3200 2800
Wire Wire Line
	3200 3350 3200 3450
Wire Wire Line
	3200 3450 3050 3450
Wire Wire Line
	3200 3450 3200 3550
Wire Wire Line
	3200 3750 3200 3850
Wire Wire Line
	3200 3850 3050 3850
Wire Wire Line
	3200 3850 3200 3950
Wire Wire Line
	3500 2400 3650 2400
Wire Wire Line
	3500 2600 3550 2600
Wire Wire Line
	3500 2800 3650 2800
Wire Wire Line
	3500 3550 3650 3550
Wire Wire Line
	3500 3750 3550 3750
Wire Wire Line
	3500 3950 3650 3950
Wire Wire Line
	3550 2200 3500 2200
Wire Wire Line
	3550 2600 3550 2200
Wire Wire Line
	3550 2600 3550 3350
Wire Wire Line
	3550 3350 3500 3350
Wire Wire Line
	3550 3750 3550 3350
Wire Wire Line
	3550 4200 3550 3750
Wire Wire Line
	3650 2400 3650 2800
Wire Wire Line
	3650 2800 3650 3100
Wire Wire Line
	3650 3100 3650 3550
Wire Wire Line
	3650 3550 3650 3950
Wire Wire Line
	4000 3100 3650 3100
Wire Wire Line
	4200 1250 4550 1250
Wire Wire Line
	4200 1350 4550 1350
Wire Wire Line
	4200 1450 4550 1450
Wire Wire Line
	4200 1550 4550 1550
Wire Wire Line
	4200 1650 4550 1650
Wire Wire Line
	4200 1750 4550 1750
Wire Wire Line
	4200 1850 4550 1850
Wire Wire Line
	4200 1950 4550 1950
Wire Wire Line
	4200 2050 4550 2050
Wire Wire Line
	4200 2150 4550 2150
Wire Wire Line
	4200 2250 4550 2250
Wire Wire Line
	4200 2350 4550 2350
Wire Wire Line
	4200 2450 4550 2450
Wire Wire Line
	4200 2550 4550 2550
Wire Wire Line
	4200 2650 4550 2650
Wire Wire Line
	4200 2750 4550 2750
Wire Wire Line
	4200 4200 4550 4200
Wire Wire Line
	4200 4300 4550 4300
Wire Wire Line
	4200 4400 4550 4400
Wire Wire Line
	4200 4500 4550 4500
Wire Wire Line
	4200 4600 4550 4600
Wire Wire Line
	4200 4700 4550 4700
Wire Wire Line
	4200 4800 4550 4800
Wire Wire Line
	4200 4900 4550 4900
Wire Wire Line
	4200 5000 4550 5000
Wire Wire Line
	4200 5100 4550 5100
Wire Wire Line
	4200 5200 4550 5200
Wire Wire Line
	4200 5300 4550 5300
Wire Wire Line
	4200 5400 4550 5400
Wire Wire Line
	4200 5500 4550 5500
Wire Wire Line
	4200 5600 4550 5600
Wire Wire Line
	4200 5700 4550 5700
Wire Wire Line
	4350 3250 4550 3250
Wire Wire Line
	4350 6200 4550 6200
Wire Wire Line
	4400 3050 4550 3050
Wire Wire Line
	4400 6000 4550 6000
Wire Wire Line
	4550 2850 4200 2850
Wire Wire Line
	4550 3050 4550 2950
Wire Wire Line
	4550 3350 4400 3350
Wire Wire Line
	4550 5800 4200 5800
Wire Wire Line
	4550 5900 4550 6000
Wire Wire Line
	4550 6300 4400 6300
Wire Wire Line
	5100 1050 4950 1050
Wire Wire Line
	5150 4000 4950 4000
Wire Wire Line
	5350 1250 5700 1250
Wire Wire Line
	5350 1350 5700 1350
Wire Wire Line
	5350 1450 5700 1450
Wire Wire Line
	5350 1550 5700 1550
Wire Wire Line
	5350 1650 5700 1650
Wire Wire Line
	5350 1750 5700 1750
Wire Wire Line
	5350 1850 5700 1850
Wire Wire Line
	5350 1950 5700 1950
Wire Wire Line
	5350 4200 5700 4200
Wire Wire Line
	5350 4300 5700 4300
Wire Wire Line
	5350 4400 5700 4400
Wire Wire Line
	5350 4500 5700 4500
Wire Wire Line
	5350 4600 5700 4600
Wire Wire Line
	5350 4700 5700 4700
Wire Wire Line
	5350 4800 5700 4800
Wire Wire Line
	5350 4900 5700 4900
Wire Bus Line
	4100 1350 4100 1550
Wire Bus Line
	4100 1550 4100 1750
Wire Bus Line
	4100 1750 4100 1950
Wire Bus Line
	4100 1950 4100 2150
Wire Bus Line
	4100 2150 4100 2350
Wire Bus Line
	4100 2350 4100 2450
Wire Bus Line
	4100 2450 4100 2650
Wire Bus Line
	4100 2650 4100 2850
Wire Bus Line
	4100 2850 4100 3000
Wire Bus Line
	4100 3000 4100 4200
Wire Bus Line
	4100 4200 4100 4400
Wire Bus Line
	4100 4400 4100 4500
Wire Bus Line
	4100 4500 4100 4700
Wire Bus Line
	4100 4700 4100 4900
Wire Bus Line
	4100 4900 4100 5000
Wire Bus Line
	4100 5000 4100 5200
Wire Bus Line
	4100 5200 4100 5400
Wire Bus Line
	4100 5400 4100 5600
Wire Bus Line
	4100 5600 4100 5750
Wire Bus Line
	4100 5750 3900 5750
Wire Bus Line
	5800 1350 5800 1550
Wire Bus Line
	5800 1550 5800 1650
Wire Bus Line
	5800 1650 5800 1850
Wire Bus Line
	5800 1850 5800 1950
Wire Bus Line
	5800 1950 5800 3700
Wire Bus Line
	5800 3700 5800 4100
Wire Bus Line
	5800 3700 6300 3700
Wire Bus Line
	5800 4100 5800 4300
Wire Bus Line
	5800 4300 5800 4500
Wire Bus Line
	5800 4500 5800 4700
Wire Bus Line
	5800 4700 5800 4800
Text Label 2850 2300 0    50   ~ 0
R1-A16
Text Label 2850 2700 0    50   ~ 0
R2-A16
Text Label 3050 3450 0    50   ~ 0
G1
Text Label 3050 3850 0    50   ~ 0
G2
Text Label 3900 3100 2    50   ~ 0
A16
Text Label 4200 2850 0    50   ~ 0
R1-A16
Text Label 4200 5800 0    50   ~ 0
R2-A16
Text Label 4300 1250 0    50   ~ 0
A0
Text Label 4300 1350 0    50   ~ 0
A1
Text Label 4300 1450 0    50   ~ 0
A2
Text Label 4300 1550 0    50   ~ 0
A3
Text Label 4300 1650 0    50   ~ 0
A4
Text Label 4300 1750 0    50   ~ 0
A5
Text Label 4300 1850 0    50   ~ 0
A6
Text Label 4300 1950 0    50   ~ 0
A7
Text Label 4300 2050 0    50   ~ 0
A8
Text Label 4300 2150 0    50   ~ 0
A9
Text Label 4300 2250 0    50   ~ 0
A10
Text Label 4300 2350 0    50   ~ 0
A11
Text Label 4300 2450 0    50   ~ 0
A12
Text Label 4300 2550 0    50   ~ 0
A13
Text Label 4300 2650 0    50   ~ 0
A14
Text Label 4300 2750 0    50   ~ 0
A15
Text Label 4300 4200 0    50   ~ 0
A0
Text Label 4300 4300 0    50   ~ 0
A1
Text Label 4300 4400 0    50   ~ 0
A2
Text Label 4300 4500 0    50   ~ 0
A3
Text Label 4300 4600 0    50   ~ 0
A4
Text Label 4300 4700 0    50   ~ 0
A5
Text Label 4300 4800 0    50   ~ 0
A6
Text Label 4300 4900 0    50   ~ 0
A7
Text Label 4300 5000 0    50   ~ 0
A8
Text Label 4300 5100 0    50   ~ 0
A9
Text Label 4300 5200 0    50   ~ 0
A10
Text Label 4300 5300 0    50   ~ 0
A11
Text Label 4300 5400 0    50   ~ 0
A12
Text Label 4300 5500 0    50   ~ 0
A13
Text Label 4300 5600 0    50   ~ 0
A14
Text Label 4300 5700 0    50   ~ 0
A15
Text Label 4400 3350 0    50   ~ 0
G1
Text Label 4400 6300 0    50   ~ 0
G2
Text Label 5450 1250 0    50   ~ 0
D0
Text Label 5450 1350 0    50   ~ 0
D1
Text Label 5450 1450 0    50   ~ 0
D2
Text Label 5450 1550 0    50   ~ 0
D3
Text Label 5450 1650 0    50   ~ 0
D4
Text Label 5450 1750 0    50   ~ 0
D5
Text Label 5450 1850 0    50   ~ 0
D6
Text Label 5450 1950 0    50   ~ 0
D7
Text Label 5450 4200 0    50   ~ 0
D0
Text Label 5450 4300 0    50   ~ 0
D1
Text Label 5450 4400 0    50   ~ 0
D2
Text Label 5450 4500 0    50   ~ 0
D3
Text Label 5450 4600 0    50   ~ 0
D4
Text Label 5450 4700 0    50   ~ 0
D5
Text Label 5450 4800 0    50   ~ 0
D6
Text Label 5450 4900 0    50   ~ 0
D7
Text HLabel 3900 5750 0    50   Input ~ 0
A[0..16]
Text HLabel 4350 3250 0    50   Input ~ 0
~E1
Text HLabel 4350 6200 0    50   Input ~ 0
~E2
Text HLabel 6300 3700 2    50   Output ~ 0
D[0..7]
$Comp
L power:VCC #PWR0251
U 1 1 63F7B400
P 4400 3050
F 0 "#PWR0251" H 4400 2900 50  0001 C CNN
F 1 "VCC" H 4300 3100 50  0000 C CNN
F 2 "" H 4400 3050 50  0001 C CNN
F 3 "" H 4400 3050 50  0001 C CNN
	1    4400 3050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0252
U 1 1 63F81D7D
P 4400 6000
F 0 "#PWR0252" H 4400 5850 50  0001 C CNN
F 1 "VCC" H 4300 6050 50  0000 C CNN
F 2 "" H 4400 6000 50  0001 C CNN
F 3 "" H 4400 6000 50  0001 C CNN
	1    4400 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0250
U 1 1 63F7B147
P 4950 1050
F 0 "#PWR0250" H 4950 900 50  0001 C CNN
F 1 "VCC" H 4967 1223 50  0000 C CNN
F 2 "" H 4950 1050 50  0001 C CNN
F 3 "" H 4950 1050 50  0001 C CNN
	1    4950 1050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0253
U 1 1 63FA63CB
P 4950 4000
F 0 "#PWR0253" H 4950 3850 50  0001 C CNN
F 1 "VCC" H 4967 4173 50  0000 C CNN
F 2 "" H 4950 4000 50  0001 C CNN
F 3 "" H 4950 4000 50  0001 C CNN
	1    4950 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0249
U 1 1 63F4894F
P 3550 4200
F 0 "#PWR0249" H 3550 3950 50  0001 C CNN
F 1 "GND" H 3555 4027 50  0001 C CNN
F 2 "" H 3550 4200 50  0001 C CNN
F 3 "" H 3550 4200 50  0001 C CNN
	1    3550 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5E19CF49
P 4950 3500
F 0 "#PWR0106" H 4950 3250 50  0001 C CNN
F 1 "GND" H 4955 3327 50  0001 C CNN
F 2 "" H 4950 3500 50  0001 C CNN
F 3 "" H 4950 3500 50  0001 C CNN
	1    4950 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0248
U 1 1 63F488E6
P 4950 6450
F 0 "#PWR0248" H 4950 6200 50  0001 C CNN
F 1 "GND" H 4955 6277 50  0001 C CNN
F 2 "" H 4950 6450 50  0001 C CNN
F 3 "" H 4950 6450 50  0001 C CNN
	1    4950 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64041E7F
P 5300 1050
AR Path="/635BEEB0/64041E7F" Ref="#PWR?"  Part="1" 
AR Path="/64041E7F" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/64041E7F" Ref="#PWR?"  Part="1" 
AR Path="/5D9F70E5/64041E7F" Ref="#PWR?"  Part="1" 
AR Path="/63F411DC/64041E7F" Ref="#PWR0254"  Part="1" 
F 0 "#PWR0254" H 5300 800 50  0001 C CNN
F 1 "GND" H 5305 877 50  0001 C CNN
F 2 "" H 5300 1050 50  0001 C CNN
F 3 "" H 5300 1050 50  0001 C CNN
	1    5300 1050
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 64044BD2
P 5350 4000
AR Path="/635BEEB0/64044BD2" Ref="#PWR?"  Part="1" 
AR Path="/64044BD2" Ref="#PWR?"  Part="1" 
AR Path="/5DC1E7F0/64044BD2" Ref="#PWR?"  Part="1" 
AR Path="/5D9F70E5/64044BD2" Ref="#PWR?"  Part="1" 
AR Path="/63F411DC/64044BD2" Ref="#PWR0255"  Part="1" 
F 0 "#PWR0255" H 5350 3750 50  0001 C CNN
F 1 "GND" H 5355 3827 50  0001 C CNN
F 2 "" H 5350 4000 50  0001 C CNN
F 3 "" H 5350 4000 50  0001 C CNN
	1    5350 4000
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 64041E79
P 5200 1050
AR Path="/635BEEB0/64041E79" Ref="C?"  Part="1" 
AR Path="/64041E79" Ref="C?"  Part="1" 
AR Path="/5DC1E7F0/64041E79" Ref="C?"  Part="1" 
AR Path="/5D9F70E5/64041E79" Ref="C?"  Part="1" 
AR Path="/63F411DC/64041E79" Ref="CX31"  Part="1" 
F 0 "CX31" V 4971 1050 50  0001 C CNN
F 1 "0.1u" V 5063 1050 50  0000 C TNN
F 2 "" H 5200 1050 50  0001 C CNN
F 3 "~" H 5200 1050 50  0001 C CNN
	1    5200 1050
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 64044BCC
P 5250 4000
AR Path="/635BEEB0/64044BCC" Ref="C?"  Part="1" 
AR Path="/64044BCC" Ref="C?"  Part="1" 
AR Path="/5DC1E7F0/64044BCC" Ref="C?"  Part="1" 
AR Path="/5D9F70E5/64044BCC" Ref="C?"  Part="1" 
AR Path="/63F411DC/64044BCC" Ref="CX32"  Part="1" 
F 0 "CX32" V 5021 4000 50  0001 C CNN
F 1 "0.1u" V 5113 4000 50  0000 C TNN
F 2 "" H 5250 4000 50  0001 C CNN
F 3 "~" H 5250 4000 50  0001 C CNN
	1    5250 4000
	0    -1   1    0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 63F4780A
P 3350 2200
AR Path="/5E05893C/63F4780A" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F4780A" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F4780A" Ref="JP1"  Part="1" 
F 0 "JP1" H 3350 2100 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3350 2314 50  0001 C CNN
F 2 "" H 3350 2200 50  0001 C CNN
F 3 "~" H 3350 2200 50  0001 C CNN
	1    3350 2200
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 63F47803
P 3350 2400
AR Path="/5E05893C/63F47803" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47803" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47803" Ref="JP2"  Part="1" 
F 0 "JP2" H 3350 2300 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 3350 2514 50  0001 C CNN
F 2 "" H 3350 2400 50  0001 C CNN
F 3 "~" H 3350 2400 50  0001 C CNN
	1    3350 2400
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 63F47818
P 3350 2600
AR Path="/5E05893C/63F47818" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47818" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47818" Ref="JP5"  Part="1" 
F 0 "JP5" H 3350 2500 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3350 2714 50  0001 C CNN
F 2 "" H 3350 2600 50  0001 C CNN
F 3 "~" H 3350 2600 50  0001 C CNN
	1    3350 2600
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 63F47811
P 3350 2800
AR Path="/5E05893C/63F47811" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47811" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47811" Ref="JP6"  Part="1" 
F 0 "JP6" H 3350 2700 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 3350 2914 50  0001 C CNN
F 2 "" H 3350 2800 50  0001 C CNN
F 3 "~" H 3350 2800 50  0001 C CNN
	1    3350 2800
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 63F47959
P 3350 3350
AR Path="/5E05893C/63F47959" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47959" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47959" Ref="JP3"  Part="1" 
F 0 "JP3" H 3350 3250 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 3350 3464 50  0001 C CNN
F 2 "" H 3350 3350 50  0001 C CNN
F 3 "~" H 3350 3350 50  0001 C CNN
	1    3350 3350
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 63F47960
P 3350 3550
AR Path="/5E05893C/63F47960" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47960" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47960" Ref="JP4"  Part="1" 
F 0 "JP4" H 3350 3450 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3350 3664 50  0001 C CNN
F 2 "" H 3350 3550 50  0001 C CNN
F 3 "~" H 3350 3550 50  0001 C CNN
	1    3350 3550
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP?
U 1 1 63F47967
P 3350 3750
AR Path="/5E05893C/63F47967" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F47967" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F47967" Ref="JP8"  Part="1" 
F 0 "JP8" H 3350 3650 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 3350 3864 50  0001 C CNN
F 2 "" H 3350 3750 50  0001 C CNN
F 3 "~" H 3350 3750 50  0001 C CNN
	1    3350 3750
	-1   0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 63F4796E
P 3350 3950
AR Path="/5E05893C/63F4796E" Ref="JP?"  Part="1" 
AR Path="/62E08D9B/63F4796E" Ref="JP?"  Part="1" 
AR Path="/63F411DC/63F4796E" Ref="JP9"  Part="1" 
F 0 "JP9" H 3350 3850 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 3350 4064 50  0001 C CNN
F 2 "" H 3350 3950 50  0001 C CNN
F 3 "~" H 3350 3950 50  0001 C CNN
	1    3350 3950
	-1   0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4001 UA1-ROM1
U 1 1 63F432ED
P 4950 2300
F 0 "UA1-ROM1" H 4650 3500 50  0000 C CNN
F 1 "27C4001" H 5150 1150 50  0000 C CNN
F 2 "Package_DIP:DIP-32_W15.24mm_Socket" H 4950 2300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 4950 2450 50  0001 C CNN
	1    4950 2300
	1    0    0    -1  
$EndComp
$Comp
L CPS2-Custom:27C4001 UA2-ROM2
U 1 1 63F433C9
P 4950 5250
F 0 "UA2-ROM2" H 4650 6450 50  0000 C CNN
F 1 "27C4001" H 5150 4100 50  0000 C CNN
F 2 "Package_DIP:DIP-32_W15.24mm_Socket" H 4950 5250 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0015.pdf" H 4950 5400 50  0001 C CNN
	1    4950 5250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
