# DL-1827 Reverse Engineering

![DL-1827](./DL-1827.jpg)

## PDF Render

[Click here to download a PDF export of the latest version](https://petitl.fr/cps2/DL-1827.pdf).

[![DL-1827](https://petitl.fr/cps2/DL-1827.svg)](https://petitl.fr/cps2/DL-1827.pdf)

## Function

This chip is in charge of the following:
* Transmitting the `OBJRAM` data bus to/from the `SRAM` chips.
* Transmitting the `OBJRAM` address bus to/from the `SRAM` chips and handle the object RAM bank selection.
* Transmitting the `OBJRAM` control signals.
* Transmitting the security injection signals to the CPU (provided by the `CN9` in later revisions).
* Manages the peripherals control signals for the CPU, provides all the bus control signals.
* Manages the interrupt signals for the CPU.

## Schematics notes

* The references of all the gates are all of the following format `UxRy`. `x` and `y` point to the coordinates in the gate array.
* The name of the gate is the name of the equivalent Fujitsu CG10 cell. One exception is the `B12` which looks and behaves like a 1-bit bus driver (`B11`) without the input transistors (maybe it's a CG24 only cell?).
* All the available gates are shown in the schematic even the buffers. That is why there are files with only buffers that transform pins like `_CPU-BG` to `CPU-BG` for instance. The used notation being that, in case of conflicts, signals with `_` as a prefix are the unbuffered direct pin.
* Please read the additional notes written in the schematics. They include information about how some pins are actually operated on the CPS2.

## Pinout

| Pin | Type | Name |
| ----- | ----- | ----- |
| 1 | GND |  |
| 2 | Bidirectional | OBJRAM-DATA15 |
| 3 | Bidirectional | OBJRAM-DATA14 |
| 4 | Bidirectional | OBJRAM-DATA13 |
| 5 | Bidirectional | OBJRAM-DATA12 |
| 6 | Bidirectional | OBJRAM-DATA11 |
| 7 | Bidirectional | OBJRAM-DATA10 |
| 8 | Bidirectional | OBJRAM-DATA9 |
| 9 | Bidirectional | OBJRAM-DATA8 |
| 10 | GND |  |
| 11 | Bidirectional | OBJRAM-DATA7 |
| 12 | Bidirectional | OBJRAM-DATA6 |
| 13 | Bidirectional | OBJRAM-DATA5 |
| 14 | Bidirectional | OBJRAM-DATA4 |
| 15 | Bidirectional | OBJRAM-DATA3 |
| 16 | Bidirectional | OBJRAM-DATA2 |
| 17 | Bidirectional | OBJRAM-DATA1 |
| 18 | Bidirectional | OBJRAM-DATA0 |
| 19 | Input | DEBUG-E |
| 20 | VCC |  |
| 21 | 3 State | /DEBUG-LDSW |
| 22 | Output | /VBLANK |
| 23 | Output | SECURITY-DATA |
| 24 | Output | /SECURITY-E1 |
| 25 | 3 State | /SECURITY-E2A |
| 26 | 3 State | /SECURITY-E2B |
| 27 | Output | /CPU-VPA |
| 28 | Output | /CPU-BGERR |
| 29 | Output | /CPU-IPL2 |
| 30 | GND |  |
| 31 | Output | /CPU-IPL1 |
| 32 | Output | /CPU-IPL0 |
| 33 | Output | /CPU-BGACK |
| 34 | Output | /CPU-BR |
| 35 | Output | /CPU-DTACK |
| 36 | 3 State | /BUS-UDSW |
| 37 | 3 State | /BUS-LDSW |
| 38 | 3 State | /BUS-R |
| 39 | 3 State | /BUS-W |
| 40 | VCC |  |
| 41 | GND |  |
| 42 | 3 State | /BUS-AS |
| 43 | 3 State | /BUS-E |
| 44 | 3 State | /BUS-BGACK |
| 45 | 3 State | /DEBUG-UDSW |
| 46 | 3 State | /DEBUG-RAM-E |
| 47 | 3 State | /SRAM-WL |
| 48 | 3 State | /SRAM-WH |
| 49 | Output | CLK16M |
| 50 | GND |  |
| 51 | 3 State | DEBUG-Q6 |
| 52 | 3 State | DEBUG-Q7 |
| 53 | Input | OBJRAM-ADDR12 |
| 54 | Input | OBJRAM-ADDR11 |
| 55 | Input | OBJRAM-ADDR10 |
| 56 | Input | OBJRAM-ADDR9 |
| 57 | Input | OBJRAM-ADDR8 |
| 58 | Bidirectional | CLK4M |
| 59 | GND |  |
| 60 | VCC |  |
| 61 | Input | OBJRAM-ADDR7 |
| 62 | Input | OBJRAM-ADDR6 |
| 63 | Input | OBJRAM-ADDR5 |
| 64 | Input | OBJRAM-ADDR4 |
| 65 | Input | OBJRAM-ADDR3 |
| 66 | Input | OBJRAM-ADDR2 |
| 67 | Input | OBJRAM-ADDR1 |
| 68 | Input | OBJRAM-ADDR0 |
| 69 | Bidirectional | SECURITY-CLK |
| 70 | GND |  |
| 71 | Input | CPU-FC2 |
| 72 | Input | CPU-FC1 |
| 73 | Input | CPU-FC0 |
| 74 | Output | DEBUG-Q5 |
| 75 | Input | /CPU-BG |
| 76 | Input | /CPU-LDSW |
| 77 | Input | /CPU-UDSW |
| 78 | Input | /CPU-AS |
| 79 | Input | CPU-R/W |
| 80 | VCC |  |
| 81 | GND |  |
| 82 | Bidirectional | SRAM-DATA15 |
| 83 | Bidirectional | SRAM-DATA14 |
| 84 | Bidirectional | SRAM-DATA13 |
| 85 | Bidirectional | SRAM-DATA12 |
| 86 | Bidirectional | SRAM-DATA11 |
| 87 | Bidirectional | SRAM-DATA10 |
| 88 | Bidirectional | SRAM-DATA9 |
| 89 | Bidirectional | SRAM-DATA8 |
| 90 | GND |  |
| 91 | Bidirectional | SRAM-DATA7 |
| 92 | Bidirectional | SRAM-DATA6 |
| 93 | Bidirectional | SRAM-DATA5 |
| 94 | Bidirectional | SRAM-DATA4 |
| 95 | Bidirectional | SRAM-DATA3 |
| 96 | Bidirectional | SRAM-DATA2 |
| 97 | Bidirectional | SRAM-DATA1 |
| 98 | Bidirectional | SRAM-DATA0 |
| 99 | 3 State | DEBUG-Q4 |
| 100 | VCC |  |
| 101 | Input | DEBUG-RANGE |
| 102 | Input | OBJRAM-BANK |
| 103 | Input | OBJRAM-BANK-CLK |
| 104 | Input | /CPSA-E |
| 105 | Input | DEBUG-S2 |
| 106 | Input | DEBUG-S1 |
| 107 | Input | DEBUG-S0 |
| 108 | 3 State | SRAM-ADDR12 |
| 109 | 3 State | SRAM-ADDR11 |
| 110 | GND |  |
| 111 | 3 State | SRAM-ADDR10 |
| 112 | 3 State | SRAM-ADDR9 |
| 113 | 3 State | SRAM-ADDR8 |
| 114 | 3 State | SRAM-ADDR7 |
| 115 | 3 State | SRAM-ADDR6 |
| 116 | 3 State | SRAM-ADDR5 |
| 117 | 3 State | SRAM-ADDR4 |
| 118 | 3 State | SRAM-ADDR3 |
| 119 | 3 State | SRAM-ADDR2 |
| 120 | VCC |  |
| 121 | GND |  |
| 122 | 3 State | SRAM-ADDR1 |
| 123 | 3 State | SRAM-ADDR0 |
| 124 | 3 State | /SRAM-OE |
| 125 | 3 State | /SRAM-E |
| 126 | 3 State | BUS-DIR |
| 127 | 3 State | /DEBUG-CPS1-E |
| 128 | 3 State | DEBUG-Q3 |
| 129 | Output | SECURITY-CLK |
| 130 | GND |  |
| 131 | Input | SECURITY-DATA |
| 132 | Input | /SECURITY-E1 |
| 133 | Input | /OBJRAM-WL |
| 134 | Input | /OBJRAM-WH |
| 135 | Input | /OBJRAM-OE |
| 136 | Input | /OBJRAM-E |
| 137 | Input | /CPU-ADDR23 |
| 138 | Bidirectional |  |
| 139 | GND |  |
| 140 | VCC |  |
| 141 | 3 State | DEBUG-Q2 |
| 142 | 3 State | DEBUG-Q1 |
| 143 | Input | /CPU-ADDR22 |
| 144 | Input | /CPU-ADDR21 |
| 145 | Input | /CPU-ADDR20 |
| 146 | Input | /CPU-ADDR19 |
| 147 | Input | /CPU-ADDR18 |
| 148 | Input | /CPU-ADDR17 |
| 149 | Input | /CPU-ADDR16 |
| 150 | GND |  |
| 151 | Input | /SECURITY-E2 |
| 152 | Input | /VBLANK |
| 153 | Input | /CPSB-OBJUP |
| 154 | Input | /RESET |
| 155 | Input | /Z80-BUS |
| 156 | Input | /CPSA-RESET |
| 157 | Input | /CPSA-BR |
| 158 | Input | /CPSA-E |
| 159 | 3 State | DEBUG-Q0 |
| 160 | VCC |  |

## Authors & Licence & Thanks
See main [README](../../README.md) and the [DL README](../README.md).
