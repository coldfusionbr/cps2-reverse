# Capcom Custom Chips Reverse Engineering

In the CPS2, there are a total of 11 chips designed by Capcom. The technologies varies depending on the provider. The usage of those is usually three-fold: compactness, obfuscation and security. Reversing these chips implies a lot of effort as it requires to: decap the chip, taking a HQ picture of the die, maybe delayering, and understanding how the die behaves. Fortunately, most are based on Gate Array technology which basically means that all the transistors are preinstalled, only the metal wiring changes. However, this still requires hundreds of hours to analyze each wire and each "cell".

For more information, please consult this [blog post by Eduardo Cruz](http://arcadehacker.blogspot.com/2017/03/a-journey-into-capcoms-cps2-silicon.html).

Note: some decap have been done, but images are not available publically yet.

## A-Board

| Name | Description | Technology | Decap | Reverse |
| ------ | ------ | ------ | ------ | ------ |
| DL-0311 | aka. CPS-A-01 available on the CPS1. Graphics processing. | GA - Ricoh A5C series | NA | 0% |
| DL-0921 | aka. CPS-B-21 available on the CPS1. Graphics processing & security. | GA - Ricoh A5C series | Yes | 0% |
| DL-1123 | I/O interface to the main bus. | GA - Hitachi HG62F Model 22 | No | 0% |
| DL-1425 | QSound processing. | AT&T DSP WEDSP16A-M14 | [Yes](https://siliconpr0n.org/map/capcom/dl-1425/) | 0% |
| DL-1625 | Video RAM interface. | GA - VLSI VGT300 | No | 0% |
| DL-2227 | Video RAM refresh (?) | GA - Hitachi HG62E Model 8 | No | 0% |

## B-Board

| Name | Description | Technology | Decap | Reverse |
| ------ | ------ | ------ | ------ | ------ |
| DL-1525 | Main processor 68000, with security and bus routing | Motorola H4C series model 057, GA + 68000 core | Yes | 0% |
| DL-1727 | Interface with the main bus and other management. | GA - Fujitsu CG24 | Yes | 0% |
| [DL-1827](./DL-1827/README.md) | Interface with the object ram and bus control. | GA - Fujitsu CG24 | Yes | 100% |
| DL-1927 | Interface with the address bus of the graphics ROM. | GA - Fujitsu CG24 | No | 0% |
| DL-2027 | Interface with the data bus of the graphics ROM. | GA - Fujitsu CG24 | Yes | 0% |

## Thanks
This work would have not been possible without the help of the following persons:

The decap work has been provided by:
* [John McMaster](https://twitter.com/johndmcmaster)
* [Eduardo Cruz](http://arcadehacker.blogspot.com/)

Additional tools and knowledge provided by:
* [furrtek](https://twitter.com/furrtek)
